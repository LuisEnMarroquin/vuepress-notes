module.exports = {
  root: true,

  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },

  env: {
    browser: true
  },

  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential',
    '@vue/standard'
  ],

  // required to lint *.vue files
  plugins: [
    'vue'
  ],

  globals: {
    'ga': true,
    'store': true,
    'process': true,
    '__statics': true,
  },

  // add your custom rules here
  rules: {
    'one-var': 'off',
    'arrow-parens': 'off', // allow paren-less arrow functions
    'import/first': 'off',
    'import/named': 'error',
    'import/export': 'error',
    'import/default': 'error',
    'import/extensions': 'off',
    'import/namespace': 'error',
    'import/no-unresolved': 'off',
    'generator-star-spacing': 'off', // allow async-await
    'prefer-promise-reject-errors': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off', // allow console.log during development only
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off' // allow debugger during development only
  }
}
