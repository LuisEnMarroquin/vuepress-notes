﻿#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn ; Enable warnings to assist with detecting common errors.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.

; ! = alt
; ^ = ctrl
; + = shift
; & = and

; alt + key
!a:: Send á
!e:: Send é
!i:: Send í
!o:: Send ó
!u:: Send ú
!n:: Send ñ
!c:: Send ç
!`:: Send |
!1:: Send ₁
!2:: Send ₂
!3:: Send ₃
!4:: Send ₄
!5:: Send ₅
!6:: Send ₆
!7:: Send ₇
!8:: Send ₈
!9:: Send ₉
!0:: Send ₀
!q:: Send ⁺⁻⁼⁽⁾

; shift + alt + key
+!a:: Send Á
+!e:: Send É
+!i:: Send Í
+!o:: Send Ó
+!u:: Send Ú
+!n:: Send Ñ
+!c:: Send Ç
+!1:: Send ¹
+!2:: Send ²
+!3:: Send ³
+!4:: Send ⁴
+!5:: Send ⁵
+!6:: Send ⁶
+!7:: Send ⁷
+!8:: Send ⁸
+!9:: Send ⁹
+!0:: Send ⁰

; ctrl + alt + key
^!a:: Send à
^!e:: Send è
^!i:: Send ì
^!o:: Send ò
^!u:: Send ù

; ctrl + shift + key
^+?:: Send ¿
^+!:: Send ¡

; ctrl + shift + alt + key
^+!a:: Send À
^+!e:: Send È
^+!i:: Send Ì
^+!o:: Send Ò
^+!u:: Send Ù
