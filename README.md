# Vuepress with gitlab-ci

VuePress is a static website generator. The most important feature of VuePress is that it naturally supports using Vue in Markdown.

* [Vuepress PWA](https://blog.howar31.com/vuepress-pwa/)
* [Vuepress tips](https://blog.logrocket.com/vuepress-in-all-its-glory-2f682e4f70c0/)
* [Manifest Example](https://github.com/vuejs/vuepress/blob/master/packages/docs/docs/.vuepress/public/manifest.json)
