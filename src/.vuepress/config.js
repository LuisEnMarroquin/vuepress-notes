module.exports = {
  title: 'Notas',
  description: 'Ingeniería en Sistemas Computacionales',
  port: '5444',
  dest: './public',
  head: [
    ['link', { rel: 'icon', href: '/favicon.png' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }]
  ],
  themeConfig: {
    sidebar: 'auto',
    nav: [
      { text: 'Horario', link: '/horario' }
    ]
  },
  markdown: {
    lineNumbers: true,
    toc: { includeLevel: [1, 2] }
  },
  plugins: [
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-145418060-2'
      }
    ]
  ],
  chainWebpack: config => {
    config.module
      .rule('pug')
      .test(/\.pug$/)
      .use('pug-plain-loader')
      .loader('pug-plain-loader')
      .end()
  }
}
