# Algebra

* Profesor: Jesús Arriaga Garza

Temas para el primer parcial
*	Introducción a los números reales
*	Propiedades de los números reales
*	Propiedades de campo
*	Potencias, logaritmos y funciones exponenciales. Aplicaciones

Evaluación
*	Actividades (8 => 1 por clase) 20%
*	Tareas (laboratorios: 8 o menos) 20%
*	Examen 60%

### Plataforma ALEKS LATAM
* Curso: `18 semanas`
* Código de clase: `X3NGW_GGRNQ`
* Código de descuento: `HE_UVM18W`
* Naturales: `1, 2, 3, 4... => Reales`
* Operaciones: `Suma y multiplicación`

## Axiomas de campo
1.	Cerradura para la suma y la multiplicación: Suma y multiplicación se satisfacen.
2.	Conmutatividad (+, *):  Se satisface para la suma a+b=b+a y la multiplicación a*b=b*a
3.	Asociatividad (+, *): el orden de los factores no altera el producto.

Construcción del conjunto de los números Reales (R) a partir del conjunto de los Naturales (N)

* Naturales = N = {1, 2, 3, 4, 5…}

Se somete N (los Naturales) a 6 axiomas de campo, bajo las operaciones de suma y multiplicación.

1.	Axioma de Cerradura (+, *). Si a y b están en N:
*	a + b también estará en N: 3 + 4 = 7
*	a * b también estará en N: 3 * 4 = 12

2.	Axioma de conmutabilidad (+, *). Si a y b están en N: (se puede cambiar el orden)
*	a + b = b + a: 3 + 4 = 4 + 3
*	a * b = b * a: 3 * 4 = 4 * 3

3.	Axioma de asociatividad (+, *). Si a, b y c están en N:
*	a + (b + c) = (a + b) + c
*	3 + (4 + 5) = (3 + 4) + 5
*	3 + 9 = 7 + 5
*	12 = 12
*	a * (b * c) = (a * b) * c
*	3 * (4 * 5) = (3 * 4) * 5
*	3 * 20 = 12 * 5
*	60 = 60

4.	Axioma de distributivilidad de la multiplicación respecto a la suma. Si a, b y c están en N:
*	a * (b + c) = a * b + a * c
*	3 * (4 + 5) = 3 * 4 + 3 * 5
*	3 * 9 = 12 + 15
*	27 = 27

5.	Axioma de los elementos neutros o identidad (+, *):
*	a + 0 = a
*	“conjunto N” U {0} = {0, 1, 2, 3, 4, 5…}
*	a * 1 = a
*	El “0” y el “1” son los elementos neutros o de identidad para la suma y la multiplicación respectivamente.

6.	Axioma de los elementos inversos (+, *)
*	Suma: a + (-a) = 0
*	Si `a` son los enteros positivos `E+` y `-a` son los enteros negativos `E-`
*	Entonces: N U {0} U E-
*	Entonces: E+ U {0} U E-
*	Multiplicación: a * (1/a) = 1
*	3 * (1/3) = 1
*	Se necesitan las fracciones de la forma {p/q} (todas las fracciones)
*	Entonces: E+ U E- U {0} U {p/q}
*	A los números que se pueden expresar como el cociente de 2 enteros se llaman RACIONALES: E + U E- U {0} U {p/q}

Los números IRRACIONALES no se pueden expresar como el cociente de 2 enteros. Por ejemplo: π o e, y sus factores -2π, -8π, 2e…
* Los números Reales son: E + U E- U {0} U {p/q} U I (Irracionales)
* REALES = RACIONALES + IRRACIONALES

## Actividad 2

Hay que expresar que axioma se aplica en cada ejercicio:
1.	Cerradura para la suma: 3 + 6 = 9
2.	Cerradura para la suma: 5 + 14 = 19
3.	Conmutabilidad para la suma: 6 + 7 = 7 + 6
4.	Conmutabilidad para la suma: 5 + 8 = 8 + 5
5.	Cerradura para la multiplicación: 3 * 8 = 24
6.	Cerradura para la multiplicación: 6 * 1 = 6
7.	Conmutabilidad para la multiplicación: 9 * 2 = 2 * 9
8.	Conmutabilidad para la multiplicación: 10 * 3 = 3 * 10
9.	Asociatividad para la suma: 2 + (3 + 6) = (2 + 3) + 6
10.	Conmutabilidad para la suma: 5 + (2 + 1) = (5 + 2) + 1
11.	Asociatividad para la multiplicación: 2 * (3 * 6) = (2 * 3) * 6
12.	Asociatividad para la multiplicación: 5 * (2 * 1) = (5 * 2) * 1
13.	Distributivilidad de la multiplicación respecto a la suma: 6 * (4 + 5) = 6 * 4 + 6 * 5
14.	Cerradura para la suma: 6 + 0 = 6
15.	Cerradura para la multiplicación: 7 * 1 = 7
16.	Elementos neutros o identidad para la suma: 4 + (-4) = 0
17.	Elementos inversos para la multiplicación: 6 * (1/6) = 1

## Regla de signos para la multiplicación y la división

### Multiplicación
*	(+) * (+) = +
*	(+) * (-) = -
*	(-) * (+) = -
*	(-) * (-) = +

### División
*	(+) / (+) = +
*	(+) / (-) = -
*	(-) / (+) = -
*	(-) / (-) = +

## Actividad 3

Efectuar las siguientes multiplicaciones.
1.	(+3) * (+4) = +12
2.	(-5) * (-3) = +15
3.	(+4) * (-5) = -20
4.	(-3) * (6) = -18
5.	(-6) * (5) = -30
6.	(1) * (-6) = -6
7.	(-3) * (-3) = +9
8.	(+3) * (-12) = -32
9.	(+4) * (+8) = +32
10.	(-4) * (-8) = +32
11.	(-5) * (-12) = +60
12.	(-8) * (-1) = +8
13.	(+7) * (-9) = -63
14.	(+5) * (-8) = -40
15.	(-8) * (-3) = +24
16.	(-9) * (+3) = -27
17.	(-7) * (+7) = -49
18.	(-6) * (-8) = +48
19.	(-4) * (-2) * (-4) = -32
20.	(+3) * (-2) * (+5) = -30
21.	(-2) * (-3) * (-4) = -24
22.	(-2) * (-2) * (-2) * (-2) = 16
23.	(-3) * (+2) * (-2) * (+1) * (-1) * (+5) = -60
24.	(-2) * (-2) * (+4) * (-5) * (+1) * (-1) = 80
25.	(-7) * (+7) * (+15) * (-3) * (-6) = -13230

Efectuar las siguientes divisiones:
1.	15 / 3 = 5
2.	-15 / -3 = 5
3.	-15 / 3 = -5
4.	15 / -3 = -5
5.	12 / -6 = -2
6.	-6 / -6 = 1
7.	-8 / 1 = -8
8.	6 / 3 = 2
9.	-24 / 8 = -3
10.	-12 / 3 = -4
11.	10 / -2 = -5
12.	-20 / 5 = -4
13.	-18 / -3 = 6
14.	18 / -3 = -6
15.	18 / 3 = 6
16.	-6 / -3 = 2
17.	4 / -4 = -1
18.	12 / -6 = -2
19.	-5 / 1 = -5
20.	-12 / 3 = -4
21.	-24 / 6 = -24
22.	24 / -8 = -3
23.	-9 / -9 = 1
24.	14 / 1 = 14
25.	-28 / 14 = -2

## Actividad 4

Operaciones combinadas:
1.	(-7 + (-4)) / (-9 * 8) = 0.152 = 11 / -72
2.	((-3) * (-4)) / -6 = -2
3.	((-3 * 5) – (-2*4)) / (-8 * 7) = 0.125 = -7 / -56
4.	((3 * 1) – (-2 * 5)) / (-3 * 2) = -2.166 = 13 / -6
5.	(-4 * -2 * -3) / (-5 * -10) = -0.48 = -24 / 50

## Fracciones
* Fracciones con igual denominador
* Fracciones con diferente denominador

## Actividad 5

![alt text](./assets/alg01.jpg "Hover text")
![alt text](./assets/alg02.jpg "Hover text")

### Escribir en lenguaje algebraico las siguientes expresiones verbales
1.	A + B
2.	A – B
3.	A * B
4.	X * Y * Z – 5
5.	3X
6.	(X/Y) * (X/Y)
7.	X/Y
8.	(X + Y) / Z
9.	(X - Y) / Z
10.	(X+Y) / (X-Y)
11.	X² + 13
12.	X³ – 6
13.	3X²
14.	2X³
15.	√(X*Y)
16.	(X + Y)²
17.	X² + Y²
18.	(X - Y)²
19.	X² - Y²
20.	(X + Y)³
21.	X³ + Y³
22.	(X - Y)³
23.	X³ - Y³
24.	X² /2
25.	(X/2)²
26.	X³ /3
27.	 (X/3)³
28.	P = A + B +C
29.	d = v * t
30.	A = b * h
31.	A = (a + b) * h
32.	X + 3 = 8
33.	X – 5 = 13
34.	X + 4 = 10 – X
35.	3X = 2Y

## Actividad 6

### Escribir en lenguaje común las siguientes expresiones algebraicas
1.	El doble de un número más otro número
2.	El producto de 3 números
3.	El producto de 2 números mas la suma de esos mismos números
4.	Un numero menos la diferencia de 2 números
5.	La suma de 2 números menos la diferencia de esos mismos números
6.	La suma de 2 números entre 10
7.	La suma de 2 números menos la diferencia de esos números entre el producto de esos números
8.	El triple del cuadrado de un numero
9.	El cuadrado de la suma de 2 números entre 2
10.	La suma de 2 números al cuadrado dividido entre 2
11.	La diferencia de los cuadrados de 2 números
12.	El perímetro es igual al triple de un lado de un triángulo equilátero
13.	El tiempo es igual a la distancia entre la velocidad
14.	El perímetro es igual al doble de la suma de 2 de los lados de un rectángulo
15.	El área es igual al cuadrado de un lado de un cuadrado

## Leyes de los exponentes
* Elementos de un término algebraico
+3x⁴ (Signo / Coeficiente o factor numérico (veces que la literal se expresa como sumando) / Base o factor literal / Exponente)

### 1ra Ley de Exponentes
* x^m * x^n = x^(m+n)
* x² * x⁵ = x^(2+5) = x⁷

### 2da Ley de Exponentes
* x^m / x^n = x^(m-n)
* x⁷ / x³ = x^(7-3) = x⁴

### 3ra Ley de Exponentes
* x⁰ = 1

### 4ta Ley de Exponentes
* (x * y)^m = x^m * y^m

### 5ta Ley de Exponentes
* (x^m)^n = x^(m*n)
* (3x²)³ = 3³ * x⁶

### 6ta Ley de Exponentes
* x^+m = 1 / x^-m
* x^-m = 1 / x^+m
* x^-5 = 1 / 3^+5

## Actividad 6

![alt text](./assets/alg03.jpg "Hover text")

1. x ^ 8
2. 5 ^ 5
3. 2 ^ 7
4. x ^ (m + n)
5. x ^ (2n + 1)
6. 3³
7. x⁵
8. x⁵
9. x ^ (m + n - r)
10. /
11. x⁶
12. 10 ^ -10
13. 10 ^ -6
14. 5 ^ -1
15. -x²⁰
16. -x²⁰
17. 10 ^ (3 / 2)
18. x ^ (m * n)
19. x ^ 6
20. 81 ^ 3/2
21. 9 (x ^ 2)
22. (4 ^ n) (x ^ n) (y ^ n)
23. 15 ^ 3 = 3375‬
24. (16xy⁶) ^ (1/2)
25. (x⁴) (y⁶)
26. 125 (x ^ 6)
27. 81 x ^ 6 y ^ 7
28. x ^ 2 / y ^ -6
29. 0
30. 0
31. (x ^ n) (y ^ n)
32. 125 * 1000⁵
33. x ^ h / y ^ h
34. 9 / 16
35. 4x / 9y
36. x ^ 2n / y ^ 3n
37. 25 / 9
38. 4 / 25
39. x ^ 2 / y⁴
40. 0
41. x ^ m / x ^ n
42. x³ / x²
43. 7⁵ / 7²
44. 10 ^ (3 / 2)
45. x ^ (m * n)
46. x⁶
47. 81 ^ 3/2
48. 9 (x ^ 2)
49. 3³
50. 34x⁵
51. 8x⁵
52. y * 3x ^ (m + n)
53. 1
54. 4y
55. 5
56. 7
57. 2
58. 0
59. 0
60. 0
61. -6
62. -5
63. 4x⁴
64. 4x⁴

## Actividad 8

1. 5 > 3
2. 4 > -1
3. -5 > -7
4. -9 > -100
5. 4 > 3
6. 4 > 0
7. 4 > -4
8. -9 < -8
9. -20 > -21
10. -10 = -10
11. 7 = 7
12. 9 > -9
13. -100 < -99
14. -1000 < -999

### Evaluar una expresión

| Expresión | Valores | Sustitución | Resultado |
|---|---|---|---|
| 3a - b | a = -5, b = -3 | 3 * -5 - (-3) | 12 |
| 7x - 3y + 4 | x = -1, y = -3 | 7 * -1 - 3 * -3 + 4 | 6 |
| 10w - 4x - 5y | w = -2, x = 0, y = -10 | 10 * -2 - 4 * 0 - 5 * -10 | 30 |
| 7 - 3a - 4b | a = -6, b = -3 | 7 - 3 * -6 - 4 * -3 | 37 |
| 10 - 3x - 3y - 4w | x = 1, y = -5, w = -2 | 10 - 3 * 1 - 3 * -5 - 4 * -2 | 30 |

### Aplicar el axioma de distributibilidad

* 4 * (6x - 8y - 3z - 1) = 24x - 32y - 12z - 4
* 7 * (3a + 6b - 8c + 2) = 21a + 42b - 56c + 14
* 3x * (2x² - 3x - 1) = 3x² - 6x² -3x
* 4w * (-6w³ + 8w² - 3w - 5) = - 16x⁴ + 32w³ - 12w² - 2a
* 6x² * (-2x² -2x -3) = 12z² -24z³ -16z

### Suma y resta de fracciones

![alt text](./assets/alg04.jpg "Hover text")

1. 9 / 5
2. 10 / 2
3. 1 / 5
4. 2 / 5
5. 1 / 4
6. 1 / 6
7. 8 / 24
8. 7 / 20
9. 3 / 6
10. 4 + 3 / 4

## Notación decimal y notación potencia base 10

* 1 000 = 1 * 10 ^ 3
* 30 000 = 3 * 10 ^ 4
* 0.000 007 = 7 * 10 ^ -6
* 0.4 * 10 ^ 8 = 40 000 000
* 5 * 10 ^ -5 = 0.000 04
* 7.12 * 10 ^ -6 = 0.000 007 12

## Actividad 9

* 1 000 000 000 000 = 1 * 10¹²
* 1 000 000 000 = 1 * 10⁹
* 1 000 000 = 1 * 10⁶
* 1 000 = 1 * 10³
* 100 = 1 * 10²
* 10 = 1 * 10¹
* 1 = 1 * 10⁰
* 0.1 = 1 * 10 ^ -1
* 0.01 = 1 * 10 ^ -2
* 0.001 = 1 * 10 ^ -3
* 0.000 1 = 1 * 10 ^ -4
* 0.000 01 = 1 * 10 ^ -5
* 0.000 000 001 = 1 * 10 ^ -9
* 0.000 000 000 001 = 1 * 10 ^ -12
* 5 = 5 * 10⁰
* 250 = 25 * 10¹
* 7580 = 758 * 10¹
* 3 500 000 = 3.5 * 10⁶
* 0.2 = 2 * 10 ^ -1
* 0.07 = 7 * 10 ^ -2
* 0.000 48 = 4.8 * 10 ^ -5
* 0.000 000 327 = 3.27 * 10 ^ -9
* 0.000 07 * 4 300 000 000 = 301 000 = 3.01 * 10⁵

## Actividad 10

Multiplicación y división de fracciones. √

![alt text](./assets/alg05.jpg "Hover text")
![alt text](./assets/alg06.jpg "Hover text")
![alt text](./assets/alg07.jpg "Hover text")
![alt text](./assets/alg08.jpg "Hover text")

1. 6 / 15 = 2 / 5
2. 10 / 15
3. -7 / -15
4. -50 / -12 = -25 / -6
5. -2 / -21
6. -35 / -16
7. 2 / 15
8. 35 / 24
9. 3 / 5
10. 8 / 9
11. 30 / 2 = 15 / 1
12. -63 / -40
13. -36 / -10
14. -99 / -8
15. -80 / -117
16. 49 / 40
17. log 54 = 1.73239375982
18. log 235 = 2.37106786227
19. log 75 = 1.87506126339
20. log 1250 = 3.09691001301
21. log 815 = 2.91009054559
22. log 900 = 2.91009054559
23. log √0 = 2.95424250944
24. log √7 = 0.42254902
25. log √5 = 0.34948500216
26. log √0.07 = -0.57745097999
27. log √0.0019 = -1.36062319952
28. log √0.00005 = -2.15051499783
29. ln 54 = 3.98898404656
30. ln 235 = 5.45958551414
31. ln 75 = 4.31748811354
32. ln 1250 = 7.1308988303
33. ln 900 = 6.80239476332
34. ln 0.07 = -2.65926003693
35. ln 0.0019 = 0
36. ln 0.0005 = -7.60090245954

## Actividad 11

Considerar las siguientes propiedades de los números reales:

1. Propiedad conmutativa de la suma (no importa el orden)
2. Propiedad asociativa de la suma (no importan los parentesis)
3. Propiedad de identidad para la suma (sumar 0 a cualquier número da como resultado el mismo número)
4. Propiedad inversa para la suma (un número sumado por su opuesto da 0)
5. Propiedad distributiva (la multiplicación de un número por una suma es igual a la suma de las multiplicaciones de dicho número por cada uno de los sumandos)
6. Propiedad conmutativa para la multiplicación (no importa el orden)
7. Propiedad asociativa para la multiplicación (no importan los parentesis)
8. Propiedad de identidad para la multiplicación (multiplicar 1 a cualquier número da como resultado el mismo número)
9. Propiedad inversa para la multiplicación (un número multiplicado por su opuesto da 0)
10. Propiedad multiplicativa del 0 (el producto de cualquier número Real x y 0 es siempre igual a 0)

![alt text](./assets/alg09.jpg "Hover text")

1. Propiedad conmutativa de la suma
2. Propiedad multiplicativa del 0
3. Propiedad asociativa para la multiplicación
4. Propiedad de inversa para la multiplicación
5. Propiedad de identidad para la suma
6. Propiedad de inversa para la multiplicación
7. Propiedad conmutativa para la multiplicación
8. Propiedad asociativa para la multiplicación
9. Propiedad de inversa para la multiplicación
10. Propiedad de multiplicativa del 0
11. Propiedad asociativa para la multiplicación
12. Propiedad conmutativa de la suma
13. Propiedad multiplicativa del 0
14. Propiedad de inversa para la multiplicación
15. Propiedad conmutativa de la suma
16. Propiedad asociativa para la multiplicación
17. Propiedad conmutativa para la multiplicación
18. Propiedad asociativa para la suma
19. Propiedad multiplicativa del 0
20. Propiedad de identidad para la multiplicación

### Evaluar

1. (-8)² = 64
2. (-4)³ = -64
3. (-5)³ = -125
4. (-8)⁴ = 4096
5. (-9)² = 81
6. (-4)³ = -64
7. (-5)⁴ = 625
8. (-8)³ = -512
9. (-7)³ = -343
10. (-9)⁵ = -59049

## Actividad 12

### Responder a lo siguiente

1. Cierta solución tiene una concentración de iones de hidrógeno de `0.000 006 53` moles por litro. Escribir este número en notación científica (Potencias base 10): `6.53*10^-6`

2. Una ballena de aleta puede pesar hasta `2.6*10^5` libras, escriba este número en notación estandar (Decimal): `260 000`

3. Un telescopio infrarrojo de un astrónomo puede detectar radiación de un rango de onda de `8.35*10^-5` metros. Escriba este número en notación estandar: `0.000 083 5`

4. El diametro de plutón en el ecuador es aproximadamente de `2 390` km. Escribir este número en notación científica: `2.39*10^3`

5. Cierta solución tiene una concentración de iones de `0.000 165` moles por litro. Escribir este número en notación científica: `1.65*10^-4`

6. Un objeto puede pesar hasta `2.6*10^-5` libras. Escribir este número en notación decimal: `0.000 026`

7. Una solución tiene una concentración de `3.54*10^-6` moles por litro. Escribir el número en notación estandar: `0.000 003 54`

8. Una ballena jorobada puede pesar hasta `110 000` libras. Escribir el número en notación científica: `1.1*10^5`

### Aplicar las leyes de los exponentes

1. x³ * x⁵ = x⁸
2. 5³ * 5² = 3125
3. 2⁴ * 2³ = 128
4. 3^-2 * 3^-1 = 0.03703703703 = 3.7*10²
5. 7⁵ * 7^-3 = 49
6. 8^-2 * 8^-5 = 4.76837*10^-7
7. 16⁰ = 0
8. (√25)⁰ = 0
9. (6 * 4)^-2 = 0.00173611111 = 1.73*10³
10. (10^-2)^3 = 0.000001 = 1*10^-6
11. (x^5)^4 = x^20
12. (x^-2)^-3 = x⁶
13. (4^2*7)⁰ = 0
14. (5*10^2)⁵ = 125 000 000 = 1.25*10⁸
15. 7⁵ / 7² = 343
16. 10² / 10^-6 = 100 000 000 = 1*10⁸
17. 2⁴ / 2³ = 2
18. 15² / 15^-2 = 50625

## Números racionales e irracionales

* Racionales: Se pueden expresar como el cociente de 2 enteros y su representación decimal es exacta o tiene periodicidad.
  * 3.72: Exacta
  * 4.833333333333333333333333333333: Periódica
  * 7.251251251251251251251251251251: Periódica

* Irracionales: No se pueden expresar como el cociente de 2 enteros y su representación decimal no es exacta, ni presenta periodicidad.
  * √2, √3, π, 3π, √7, √9, √13
  * 2.398269732034623976473290564387
  * Y todos los posibles múltiplos o submúltiplos de ellos

## Actividad 13

| Número | Racional | Irracional |
|---|:-:|:-:|
| 2√6 | ✘ | ✔ |
| -8.2828... | ✔ | ✘ |
| 24.24 | ✔ | ✘ |
| -15π | ✘ | ✔ |
| -(17/2) | ✔ | ✘ |
| -9π | ✘ | ✔ |
| -42.7575... | ✔ | ✘ |
| -√13 | ✘ | ✔ |
| √25 | ✔ | ✘ |
| -16π | ✘ | ✔ |
| -88.28 | ✔ | ✘ |
| -2√3 | ✘ | ✔ |
| -√9 | ✔ | ✘ |
| -52.4646... | ✔ | ✘ |
| -22.8080... | ✔ | ✘ |
| 4π | ✘ | ✔ |
| -√16 | ✔ | ✘ |
| -√2 | ✘ | ✔ |
| -42.76 | ✔ | ✘ |
| 20π | ✘ | ✔ |
| -52.46 | ✔ | ✘ |
| -3√2 | ✘ | ✔ |
| -43.8181... | ✔ | ✘ |
| √36 | ✔ | ✘ |
| 52.6363... | ✔ | ✘ |
| 12π | ✘ | ✔ |
| -22.81 | ✔ | ✘ |
| 2√3 | ✘ | ✔ |
| 12/11 | ✔ | ✘ |
| -7.3131... | ✔ | ✘ |
| -3π | ✘ | ✔ |
| √19 | ✘ | ✔ |
| 63.5757... | ✔ | ✘ |

## Expresiones exponenciales logarítmicas

Logaritmo de un número es igual al exponente del cual hay que elevar una base para obtener dicho número

* Forma exponencial: `a ^ m = b`
* Forma logarítmica: `logₐ = m`

## Actividad 14

Pasar a representación logarítmica

1. 2³ = 8 `->` log₂ 8 = 3
2. 3² = 9 `->` log₃ 9 = 2
3. 4² = 16 `->` log₄ 16 = 2
4. 7² = 49 `->` log₇ 49 = 2
5. 10² = 100 `->` log₁₀ 100 = 2
6. 6² = 36 `->` log₆ 36 = 2
7. 8² = 64 `->` log₈ 64 = 2
8. 2⁴ = 16 `->` log₂ 16 = 4
9. 3³ = 27 `->` log₃ 27 = 3
10. 10³ = 1000 `->` log₁₀ 1000 = 3

## Propiedades de los logarítmos

* logₐ (x*y) = logₐ x + logₐ y
* logₐ (x/y) = logₐ x - logₐ y
* logₐ xⁿ = n logₐ x

![alt text](./assets/alg10.jpg "Hover text")

### Utilizar las propiedades de los logaritmos para evaluar cada expresión

![alt text](./assets/alg11.jpg "Hover text")

1. log₂ 36 - 2 log₂ 3 = log₂ (36/9) = log₂ 4 = 2
2. logₑ e⁻⁸ - logₑ e³ = logₑ e⁻⁸⁺³ = logₑ e⁻⁵ = -5
3. log₆ 9 + 2 log₆ 2 = log₆ (9*4) = log₆ 36 = 2
4. logₑ e⁸ - logₑ e² = logₑ (e⁸/e²) = logₑ e⁶ = 6
5. 2 log₁₂ 2 + log₁₂ 3 = log₁₂ (2² * 3¹) = log₁₂ 12 = 1
6. logₑ e² - logₑ e¹¹ = logₑ (e²/e¹¹) = logₑ e⁻⁹ = -9
7. log₃ 72 - 3 log₃ 2 = log₃ (72/8) = log₃ 9 = 2
8. logₑ e⁶ + logₑ e⁻¹² = logₑ e⁶⁻¹² = log e⁻⁶ = -6

## Funciones cuadráticas

* y = f(x) = Ax² + Bx + C

![alt text](./assets/alg12.jpg "Hover text")

En los puntos de corte con el eje `x`, la `y` vale `0`: (X₁, 0), (X₂, 0)

* Ecuación cuadrática general: Ax² + Bx + C = 0
La solución arroja los valores de x₁ y x₂

![alt text](./assets/alg13.jpg "Hover text")

Ejemplo

![alt text](./assets/alg14.jpg "Hover text")

1. <General A='1' B='7' C='12' />
2. <General A='1' B='-3' C='-10' />
3. <General A='1' B='10' C='9' />

## Actividad 3

Aplicar la formula general para obtener las raíces o soluciones de ecuaciones cuadráticas

1. <General A='2' B='-6' C='-8' />
2. <General A='2' B='-4' C='-30' />
3. <General A='2' B='-3' C='1' />
4. <General A='3' B='10' C='-5' />
5. <General A='5' B='-3' C='-36' />
6. <General A='4' B='7' C='3' />
7. <General A='6' B='-3' C='-18' />
8. <General A='7' B='-9' C='2' />

## Actividad 4

1. Si a un número se le agrega su cuadrado se obtiene 90. Hallar el número
  <General A='1' B='1' C='-90' />
2. Hallar el número cuyo cuadrado disminuido en el doble del número da 15
  <General A='1' B='-2' C='-15' />
3. Encontrar el número cuyo duplo de su cuadrado disminuido en el número es igual a 45
  <General A='2' B='-1' C='-45' />
4. Hallar 2 números que sumados den 12 y multiplicados den 35
  <General A='1' B='-12' C='35' />
5. La suma de 2 números es 14 y la suma de sus cuadrados es 106. ¿Cuáles son esos números?
  <General A='2' B='-28' C='90' />
6. La suma de los cuadrados de 3 números naturales consecutivos es 110- ¿Cuáles números son?
  <General A='5' B='6' C='7' />
7. El producto de 2 números enteros consecutivos es 600. Hallar los números.
  <General A='1' B='1' C='-600' />
8. Encontrar el número cuyo quintuple aumentado en 500 es igual a su cuadrado
  <General A='1' B='-5' C='-500' />
9. Hallar 2 números pares consecutivos tales que la diferencia de sus cuadrados sea 116
  <General A='1' B='28' C='30' />
10. Hallar un número tal que la mitad de su cuadrado disminuida en 8 dé 120
  <General A='1' B='-8' C='-20' />
11. El perímetro de un rectángulo es de 140 m y el área es de 1200 m². Hallar sus dimensiones
  <General A='1' B='1' C='1' />
12. Un lado del rectángulo excede al ancho en 10 m y el área es de 2000 m². Hallar sus dimensiones
  <General A='1' B='1' C='1' />
13. Si al largo de un rectángulo se le restan 3 m se obtiene un cuadrado de 225 m² de área. Hallar las dimensiones y el área del rectángulo
  <General A='1' B='1' C='1' />
14. La base de un rectángulo es el doble de su altura y el área es de 228 m². Calcular sus dimensiones
  <General A='1' B='1' C='1' />
15. En un triangulo rectángulo el cateto mayor excede en 2 cm al menor y la hipotenusa supera en 2 cm al cateto mayor. Calcular la medida de cada lado
  <General A='1' B='1' C='1' />
16. Calcular el lado de un cuadrado cuya área disminuida en el producto del lado por 5 es igual a 126 m²
  <General A='1' B='1' C='1' />
17. La banqueta que rodea a un jardín rectángular es de 3 m de ancho. El jardín tiene 10 m más de largo que de ancho. Si el área del jardín es de 1496 m², ¿cúal es la longitud del lado exterior de la banqueta?
  <General A='1' B='1' C='1' />
18. Hallar el lado de un cuadrado; si si área se aumenta en el producto de dicho lado por 5 se hace igual a 500 m²
  <General A='1' B='1' C='1' />
19. En el octágono regular el apotema excede en 2 cm al lado del polígono. Si el área es de 320 cm², hallar las medidas del lado y de el apotema.
  <General A='1' B='1' C='1' />
20. El área de un trapecio es de 84 cm². La base mayor es el doble de la menor y la altura excede en 1 cm a la base menor. Determine la medida de cada una de ellas
  <General A='1' B='1' C='1' />

![alt text](./assets/alg15.jpg "Hover text")
![alt text](./assets/alg16.jpg "Hover text")

## Suma de complejos

* (A + Bi) + (C + Di) = (A + C) + (B + D)i

1. <SumaComplejos A='3' B='5' C='7' D='5' />
1. <SumaComplejos A='10' B='2' C='20' D='8' />
1. <SumaComplejos A='-2' B='3' C='-20' D='5' />
1. <SumaComplejos A='-8' B='-7' C='-7' D='-3' />
1. <SumaComplejos A='9' B='-12' C='-9' D='8' />

## Multiplicación de complejos

![alt text](./assets/alg17.jpg "Hover text")

1. <MultComplejos A='3' B='5' C='7' D='5' />
1. <MultComplejos A='2' B='7' C='-3' D='-6' />
1. <MultComplejos A='-5' B='-4' C='-7' D='3' />
1. <MultComplejos A='-10' B='20' C='7' D='-3' />
1. <MultComplejos A='-8' B='4' C='-1' D='-3' />
1. <MultComplejos A='10' B='-3' C='-2' D='8' />

## Complejos conjugados

Si +3+5i es un complejo, su complejo conjugado será +3-5i, se cambia el signo de la parte imaginaria

### Actividad 5

1. <ConjComplejos A='6' B='5' />
1. <ConjComplejos A='10' B='7' />
1. <ConjComplejos A='-4' B='2' />
1. <ConjComplejos A='-8' B='13' />
1. <ConjComplejos A='-9' B='-8' />
1. <ConjComplejos A='-12' B='-4' />
1. <ConjComplejos A='15' B='-8' />
1. <ConjComplejos A='20' B='-15' />

## División de complejos

1. <DiviComplejos A='3' B='-5' C='-4' D='3' />
1. <DiviComplejos A='-7' B='-3' C='4' D='6' />
1. <DiviComplejos A='8' B='-5' C='7' D='8' />
1. <DiviComplejos A='9' B='-10' C='10' D='12' />
1. <DiviComplejos A='4' B='6' C='11' D='12' />
1. <DiviComplejos A='9' B='11' C='12' D='-3' />

## Actividad 6

### Suma de complejos

1. <SumaComplejos A='3' B='-7' C='10' D='-8' />
2. <SumaComplejos A='-4' B='-6' C='-2' D='-3' />
3. <SumaComplejos A='10' B='-5' C='-2' D='7' />
4. <SumaComplejos A='-7' B='8' C='-8' D='-3' />
5. <SumaComplejos A='11' B='-2' C='7' D='-3' />

### Resta de complejos

6. <RestComplejos A='7' B='2' C='-3' D='4' />
7. <RestComplejos A='11' B='-4' C='12' D='6' />
8. <RestComplejos A='9' B='-8' C='4' D='-6' />
9. <RestComplejos A='4' B='4' C='7' D='-7' />
10. <RestComplejos A='-11' B='2' C='-3' D='4' />

### Multiplicación de complejos

11. <MultComplejos A='7' B='-3' C='-8' D='-6' />
12. <MultComplejos A='10' B='4' C='-6' D='-7' />
13. <MultComplejos A='9' B='-6' C='10' D='-4' />
14. <MultComplejos A='10' B='-3' C='7' D='6' />
15. <MultComplejos A='-4' B='-6' C='-8' D='-7' />

### División de complejos

16. <DiviComplejos A='-4' B='8' C='-6' D='-7' />
17. <DiviComplejos A='11' B='-10' C='-3' D='-11' />
18. <DiviComplejos A='-5' B='-3' C='-8' D='-2' />
19. <DiviComplejos A='-7' B='2' C='-9' D='-3' />
20. <DiviComplejos A='-8' B='-12' C='-2' D='-7' />

## Actividad 7

* 2 multiplicaciones y dividir sus resultados
  1. <MultComplejos A='3' B='7' C='-2' D='-5' />
  1. <MultComplejos A='-3' B='-11' C='2' D='4' />
  1. <DiviComplejos A='-10' B='24' C='-24' D='-10' />

* 2 multiplicaciones y dividir sus resultados
  1. <MultComplejos A='-4' B='-6' C='-2' D='-3' />
  1. <MultComplejos A='-3' B='2' C='4' D='6' />
  1. <DiviComplejos A='-10' B='-24' C='-24' D='-10' />

* 2 multiplicaciones y dividir sus resultados
  1. <MultComplejos A='-3' B='7' C='-5' D='2' />
  1. <MultComplejos A='-4' B='2' C='-8' D='3' />
  1. <DiviComplejos A='1' B='-41' C='26' D='-28' />

### Gráficas de números complejos

![alt text](./assets/alg18.jpg "Hover text")

## Gráfica de complejos

Se llama módulo al número complejo Z
* <GrafComplejos A='3' B='4' />

## Actividad 8

Dados los siguientes complejos (Z)
1. Graficar
2. Calcular módulo (Z)
3. Calcular argumento (θ)

![alt text](./assets/alg19.jpg "Hover text")

1. <GrafComplejos A='3' B='7' />
2. <GrafComplejos A='5' B='2' />
3. <GrafComplejos A='8' B='3' />
4. <GrafComplejos A='-3' B='2' />
5. <GrafComplejos A='-6' B='3' />
6. <GrafComplejos A='-5' B='7' />
7. <GrafComplejos A='-4' B='-7' />
8. <GrafComplejos A='-2' B='-3' />
9. <GrafComplejos A='-8' B='-2' />
10. <GrafComplejos A='4' B='-8' />
10. <GrafComplejos A='7' B='-3' />

## Forma binómica de un complejo Z

* X = a + bi
* Módulo (Z) = |Z| = √(a² + b²)
* Argumento (Z) = θ
  * θ = tg⁻¹ (a / b)
  * θ = cos⁻¹ (a / |Z|)
  * θ = sen⁻¹ (b / |Z|)

* cos θ = a / |Z| => a = |Z| cos θ
* sen θ = b / |Z| => b = |Z| sen θ

| - | Forma binómica | Forma polar |
|:-:|:-:|:-:|
| Z | a + bi | \|Z\| cos θ + \|Z\| sen θ i |

## Actividad 9

Expresar en forma polar los siguientes complejos

1. <GrafComplejos A='4' B='6' />
2. <GrafComplejos A='5' B='2' />
3. <GrafComplejos A='-4' B='-2' />
4. <GrafComplejos A='-6' B='-7' />

* Expresar la forma binómica de los siguientes complejos

5. Z = 3 (cos 45° + sen 45° i) = Z = 2.12 + 2.12i
6. Z = 7 (cos 60° + sen 60° i) = Z = 3.50 + 6.06i


## Actividad 10

1. Escribir en términos de i y simplificar la respuesta

* √-32 = √16 * √2 * √-1 = 4i√2
* √-75 = √25 * √3 * √-1 = 5i√3
* √-80 = √40 * √5 * √-1 = 4i√5
* √-54 = √27 * √6 * √-1 = 3i√6
* √-8 = √4 * √2 * √-1 = 2i√2

2. No dejar número negativos dentro de los rádicales, ni radicales en los denominadores

* √-48 / √-6 = √8 = 2√2
* √-8 * √2 = √-16 = 4i
* √-75 / √-3 = √25 = 5
* √-11 * √7 = √-77 = i√77
* √7 * √-5 = i√35
* √-70 / √10 = √-7 = i√7
* √10 * √-3 = i√30
* √-15 / √5 = √-3 = i√3
* √-2 * √-3 = √6
* √-180 / √10 = √-18 = 3i√2

3. Suma y Resta

* <RestComplejos A='-3' B='-3' C='-6' D='5' />
* <RestComplejos A='-5' B='-4' C='2' D='4' />
* <RestComplejos A='-4' B='6' C='-2' D='2' />
* <SumaComplejos A='5' B='-5' C='6' D='-3' />
* <SumaComplejos A='4' B='-4' C='-6' D='-3' />

4. Multiplicación

* <MultComplejos A='-4' B='-6' C='2' D='2' />
* <MultComplejos A='2' B='-6' C='5' D='-4' />
* <MultComplejos A='5' B='-5' C='2' D='6' />
* <MultComplejos A='4' B='-3' C='6' D='4' />
* <MultComplejos A='-6' B='6' C='-4' D='3' />

5. Dividir

* <DiviComplejos A='1' B='-6' C='-3' D='-2' />
* <DiviComplejos A='1' B='-4' C='3' D='-4' />
* <DiviComplejos A='-2' B='3' C='3' D='4' />
* <DiviComplejos A='2' B='2' C='3' D='-4' />
* <DiviComplejos A='-3' B='3' C='5' D='-2' />

6. Gráficar

* <GrafComplejos A='3' B='-1' />
* <GrafComplejos A='-7' B='5' />
* <GrafComplejos A='6' B='-1' />
* <GrafComplejos A='-2' B='8' />
* <GrafComplejos A='2' B='-1' />
* <GrafComplejos A='-4' B='8' />
* <GrafComplejos A='-1' B='1' />
* <GrafComplejos A='6' B='-3' />
* <GrafComplejos A='-2' B='1' />
* <GrafComplejos A='3' B='-6' />

![alt text](./assets/alg20.jpg "Hover text")

## Actividad 11

* Todos son números reales. Simplificar la respuesta

1. v² = 36
v = ±6

2. v² = 16
v = ±4

3. w² = 49
w = ±7

4. x² = 36
x = ±6

5. y² = 25
y = ±5

* Simplificar el número complejo lo más que se pueda

6. 1⁴⁵ = (i²)²² * (i) = (-1)²² * i = i
7. 1²⁹ = (i²)¹⁴ * (i) = i
8. 1⁵⁵ = (i²)²⁷ * (i) = (-1)²⁷ * i = -i
9. 1⁴³ = (i²)²¹ * (i) = (-1)²¹ * i = -i
10. 1⁴⁶ = (i²)²³ = -1

* Simplificar esto

11. [x - (4 - 3i)] * [x - (4 - 3i)] = x² - 8x + 25
12. [x - (6 - 2i)] * [x - (6 - 2i)] = x² - 12x + 40
13. [x - (2 - 5i)] * [x - (2 - 5i)] = x² - 4x + 29
14. [x - (4 - 5i)] * [x - (4 - 5i)] = x² - 8x + 41
15. [x - (5 - 2i)] * [x - (5 - 2i)] = x² - 10x + 29

* Resolver para x

16. 4x² - 24x = 0 `=>` 4x² (x - 6) = 0
4x = 0 | x - 6 = 0
x₁ = 0 | x₂ = 6

17. 3x² + 21x = 0 `=>` 3x² (x + 7) = 0
3x = 0 | x + 7 = 0
x₁ = 0 | x₂ = -7

18. 5x² + 10x = 0 `=>` 5x² (x + 2) = 0
5x = 0 | x + 2 = 0
x₁ = 0 | x₂ = -2

19. 3x² - 18x = 0 `=>` 3x² (x - 6) = 0
3x = 0 | x - 6 = 0
x₁ = 0 | x₂ = 6

20. 6x² - 24x = 0 `=>` 6x² (x - 4) = 0
6x = 0 | x - 4 = 0
x₁ = 0 | x₂ = 4


## Actividad 12

* Resolver para X

1. (x + 6)² = 2x² + 22x +
  * x² + 12x + 36 = 2² + 22x + 60
  * x₁ = -6
  * x₂ = -4

2. (x - 5)² = 2x² - 6x + 28
  * x² - 10x + 25 = 2x² - 6x + 28
  * x₁ = -3
  * x₂ = -1

3. (x - 2)² = 2x² - 13x + 24
  * x² - 4x + 4 = 2x² - 13x + 24
  * x₁ = 4
  * x₂ = 5

4. (x - 3)² = 2x² - 16x + 30
  * x² - 6x + 9 = 2x² - 16x + 30
  * x₁ = 3
  * x₂ = 7

5. (x + 2)² = 2x² + 13x + 18
  * x² + 4x + 4 = 2x² + 13x + 18
  * x₁ = -7
  * x₂ = -2

6. (x + 2)² - 50 = 0
  * (x + 2)² = 50
  * x + 2 = ± √50
  * x = - 2 ± √50
  * x₁ = - 2 + √50
  * x₂ = - 2 - √50

7. (x + 6)² - 32 = 0
  * (x + 6)² = 32
  * x + 6 = ± √32
  * x = - 6 ± √32
  * x₁ = - 6 + √32
  * x₂ = - 6 - √32

8. (x - 10)² - 18 = 0
  * (x - 10)² = 18
  * x - 10 = ± √18
  * x = 10 ± √18
  * x₁ = 10 + √18
  * x₂ = 10 - √18

9. (x - 2)² - 80 = 0
  * (x - 2)² = 80
  * x - 2 = ± √80
  * x = 2 ± √80
  * x₁ = 2 + √80
  * x₂ = 2 - √80

10. (x + 10)² - 45 = 0
  * (x + 10)² = 45
  * x + 10 = ± √45
  * x = - 10 ± √45
  * x₁ = - 10 + √45
  * x₂ = - 10 - √45

11. ³√x = 2
  * x = 2³
  * x = 8

12. ⁴√x = 3
  * x = 3⁴
  * x = 81

13. ³√x = 3
  * x = 3³
  * x = 27

14. ⁴√x = 2
  * x = 2⁴
  * x = 16

15. Kira está corriendo en una carretera. Ella corre 11.2 millas a una velocidad de 7 millas por hora ¿Cuantás horas corre?
  * Distancia = Velocidad * Tiempo
  * Tiempo = Distancia / Velocidad = 11.2 millas / 7 millas por hora = 1.6 horas

16. Deandra saliá a caminar. Él se tardó 3 horas en caminar 7.5 millas. ¿Cuál es su velocidad?
  * Velocidad = Distancia * Tiempo = 3 horas * 7.5 millas = 1 hora * 2.5 millas = 2.5 millas por hora

17. Una tortuga camina en el desierto. Camina 7.5 metros en 3 minutos. ¿Cuál es su velocidad?
  * Velocidad = Distancia * Tiempo = 3 minutos * 7.5 metros = 1 hora * 150 metros = 150 metros por hora

18. Una tortuga camina en el desierto. Camina 17.5 metros a una velocidad de 7 metros por minuto. ¿Cuantos minutos camina?
  * 17.5 metros / 7 metros por minuto = 2.5 minutos

## Actividad 13

1. El área de un rectángulo de 21 ft² y la longitud del rectángulo es 1 ft menos que el doble del ancho. Hallar las dimensiones del rectángulo
* 2x - 1
* x (2x - 1) = 21
* 2x² - x - 21 = 0
* x₁ = 3
* x₂ = 3.5

2. El área de un rectángulo de 44 ft² y la longitud del rectángulo es 3 ft menos que el doble del ancho. Hallar las dimensiones del rectángulo
* 2x² - 3x = 44
* 2x² - 3x - 44 = 0
* (2x - 3) x = 2x² - 3x
* x₁ = 5.5
* x₂ = -4

3. La longitud del rectángulo es 5 yd más que el doble del ancho, y el área es de 42 yd². Hallar las dimensiones del rectángulo
* 2x² + 5x = 42
* 2x² + 5x -42 = 0
* x₁ = 3.5
* x₂ = -6

4. Lanzan una pelota de una altura inicial de 4 pies con una velocidad de 33 pies/segundo, la altura h (en pies) después de t segundos se obtiene con: `h = 4 + 3t - 16t²`. Hallar los valores de t para los cuales la altura h de la pelota es 20 pies
* h = 20
* h = 4 + 33t - 16t²
* t = 1.28
* -16t² + 33t + 6 = 0
* t₂ = 0.77

5. Lanzan un cohete con una velocidad de 235 ft/segundo. La altura h (en pies) después de t seg se obtiene con: `h = 235t - 16t²`. Hallar los valores de t para **h = 151 ft**
* h = 235t - 16t²
* -16t² + 235 - 151 = 0
* x = √427 = 20.66

6. Un edificio que mide 27 m proyecta una sombra. La distancia desde la cima del edificio a la punta de la sombra es 34 m. Hallar la longitud de la sombra
* x² + (27)² = 34²
* x = √427 = 20.66

7. Un papalote que vuela en el aire tiene una línea que mide 11 ft. Su linea se tensa y proyecta una sombra de 8ft. Hallar la altura del papalote.
![alt text](./assets/alg21.jpg "Hover text")
* h² + 8² = 11²

8. Un papalote que vuela en el aire tiene una línea que mide 12 ft. Su linea se tensa y proyecta una sombra de 11ft. Hallar la altura del papalote.
![alt text](./assets/alg22.jpg "Hover text")
* h² + 11² = 12²
* √(141 - 121)
* √23 = 4.79

9. Convertir el enunciado en ecuación. La suma de 2 por un número y 6 es equvalente a 7
* 2 * (w - 2) = 7

10. Convertir el enunciado en ecuación. El doble de la diferencia entre un número y 2 es equivalente a 7
* 2x + 6 = 7

## Actividad 14

1. A continuación se muestran los diagramas de dispersión de 4 conjuntos de datos. Contestar las siguientes preguntas

![alt text](./assets/alg23.jpg "Hover text")
![alt text](./assets/alg24.jpg "Hover text")

* a) Seleccionar el conjunto de datos que tiene un valor atípico
  * Figura 1

* b) Seleccionar los 2 conjuntos que muestran agrupamiento
  * Figura 2
  * Figura 4

2. El diagrama de Venn muestra las membrecias del club de Voleibol y del club de tenis. Utilizar el diagrama para contestar las preguntas a continuación

![alt text](./assets/alg25.jpg "Hover text")

* a) ¿Cuantas preguntas juegan tenis? **6**
* b) ¿Cuantas personas juegan Voleibol y tenis? **2**
* c) ¿Cuantas personas juegan Voleibol pero no tenis? **3**

3. Si la altura de vista de una persona es **h** metros sobre el nivel del mar, este puede ver hasta **d** kilómetros en el horizonte, donde **d = 3.6√h**. Supongamos que una persona puede ver hasta **d = 18.9 kilómetros** en el horizonte. ¿Cuál es la altura **h** sobre el nivel del mar de su vista? `18.9 = 3.6√h`

* √h = 18.9/3.6 `=>` (√h)² = (18.9/3.6)² = 27.56

4. Resolver para **v**

![alt text](./assets/alg26.jpg "Hover text")

* (4v + 3) * (2v + 1) = 0
* v¹ = -3/4
* v₂ = -1/2

## Actividad 1

Polinomios

1. (7x² - 3x - 7) + (6x² - 5x + 1) = 13x² - 8x - 1
2. (3x² - 2x - 1) + (5x² - 4x + 1) = 8x² - 6x - 6
3. (-4x² + 6x - 1) + (2x² + 3x + 5) = 24x² + 2x + 4
4. (6x² - 3x - 4) + (2x² - 5x + 7) = 8x² - 8x + 3
5. (7x² - 4x - 5) + (1x² - 3x + 2) = 8x² - 7x - 3
6. (7x² - 3x - 7) + (6x² - 5x + 1) = 13x² - 8x - 1
7. (3x² - 2x - 1) + (5x² - 4x + 1) = 8x² - 6x - 6
8. (-4x² + 6x - 1) + (2x² + 3x + 5) = 24x² + 2x + 4
9. (6x² - 3x - 4) + (2x² - 5x + 7) = 8x² - 8x + 3
10. (7x² - 4x - 5) + (1x² - 3x + 2) = 8x² - 7x - 3
11. (7x² - 3x - 7) + (6x² - 5x + 1) = 13x² - 8x - 1
12. (3x² - 2x - 1) + (5x² - 4x + 1) = 8x² - 6x - 6
13. (-4x² + 6x - 1) + (2x² + 3x + 5) = 24x² + 2x + 4
14. (6x² - 3x - 4) + (2x² - 5x + 7) = 8x² - 8x + 3
15. (7x² - 4x - 5) + (1x² - 3x + 2) = 8x² - 7x - 3

## Actividad 2

1. vx - 3v - 4x + 12 = (x - 3) (v - 4) = v (x - 3) - 4 (x + 3)
2. vw - 3v - 4x + 12 = (x - 3) (v - 4) = v (x - 3) - 4 (x + 3)
3. vx - 3v - 4x + 12 = (x - 3) (v - 4) = v (x - 3) - 4 (x + 3)
4. vx - 3v - 4x + 12 = (x - 3) (v - 4) = v (x - 3) - 4 (x + 3)
5. vx - 3v - 4x + 12 = (x - 3) (v - 4) = v (x - 3) - 4 (x + 3)

### Factorizar

6. (7x² - 3x - 7) + (6x² - 5x + 1) = 13x² - 8x - 1
7. (3x² - 2x - 1) + (5x² - 4x + 1) = 8x² - 6x - 6
8. (-4x² + 6x - 1) + (2x² + 3x + 5) = 24x² + 2x + 4
9. (6x² - 3x - 4) + (2x² - 5x + 7) = 8x² - 8x + 3
10. (7x² - 4x - 5) + (1x² - 3x + 2) = 8x² - 7x - 3

## Actividad 3

1. x² + 10x + 18 - x + 7 = x - 7
  * -x + 3 / √(x² + 10x + 18)
  * -x² - 7x / 3x + 18
  * -3x - 21 / -3
  * Cociente: x + 3
  * Residuo: -3

2. x² + 10x + 18 - x + 7 = x - 7
  * -x + 3 / √(x² + 10x + 18)
  * -x² - 7x / 3x + 18
  * -3x - 21 / -3
  * Cociente: 3x + 4
  * Residuo: -2

3. x² + 10x + 18 - x + 7 = x - 7
  * -x + 3 / √(x² + 10x + 18)
  * -x² - 7x / 3x + 18
  * -3x - 21 / -3
  * Cociente: 2x + 3
  * Residuo: 5

4. x² + 10x + 18 - x + 7 = x - 7
  * -x + 3 / √(x² + 10x + 18)
  * -x² - 7x / 3x + 18
  * -3x - 21 / -3
  * Cociente: 4x + 5
  * Residuo: -2

5. x² + 10x + 18 - x + 7 = x - 7
  * -x + 3 / √(x² + 10x + 18)
  * -x² - 7x / 3x + 18
  * -3x - 21 / -3
  * Cociente: 4x + 5
  * Residuo: -2

## Actividad 4

1. <General A='2' B='-6' C='-8' />
2. <General A='2' B='-4' C='-30' />
3. <General A='2' B='-3' C='1' />
4. <General A='3' B='10' C='-5' />
5. <General A='5' B='-3' C='-36' />
6. <General A='4' B='7' C='3' />
7. <General A='6' B='-3' C='-18' />
8. <General A='7' B='-9' C='2' />

## Actividad 5

Hallar los polinomios f(x) de grado 3 que tienen las siguientes raíces o ceros
![alt text](./assets/alg27.jpg "Hover text")
![alt text](./assets/alg28.jpg "Hover text")
![alt text](./assets/alg29.jpg "Hover text")

## Actividad 6

![alt text](./assets/alg30.jpg "Hover text")
![alt text](./assets/alg31.jpg "Hover text")
![alt text](./assets/alg32.jpg "Hover text")

## Actividad 7

1. (7x² - 3x - 7) + (6x² - 5x + 1) = 13x² - 8x - 1
2. (3x² - 2x - 1) + (5x² - 4x + 1) = 8x² - 6x - 6
3. (-4x² + 6x - 1) + (2x² + 3x + 5) = 24x² + 2x + 4
4. (6x² - 3x - 4) + (2x² - 5x + 7) = 8x² - 8x + 3
5. (7x² - 4x - 5) + (1x² - 3x + 2) = 8x² - 7x - 3
6. (7x² - 3x - 7) + (6x² - 5x + 1) = 13x² - 8x - 1
7. (3x² - 2x - 1) + (5x² - 4x + 1) = 8x² - 6x - 6
8. (-4x² + 6x - 1) + (2x² + 3x + 5) = 24x² + 2x + 4
9. (6x² - 3x - 4) + (2x² - 5x + 7) = 8x² - 8x + 3
10. (7x² - 4x - 5) + (1x² - 3x + 2) = 8x² - 7x - 3
11. (7x² - 3x - 7) + (6x² - 5x + 1) = 13x² - 8x - 1
12. (3x² - 2x - 1) + (5x² - 4x + 1) = 8x² - 6x - 6
13. (-4x² + 6x - 1) + (2x² + 3x + 5) = 24x² + 2x + 4
14. (6x² - 3x - 4) + (2x² - 5x + 7) = 8x² - 8x + 3
15. (7x² - 4x - 5) + (1x² - 3x + 2) = 8x² - 7x - 3

## Actividad 8

Determinar si cada par ordenado (x, y) es una desigualdad.

1. 4x - 6y > -18
(8, 5), (0, 3), (5, -3), (-9, -2)

2. 9x + 4y > 14
(-2, 8), (1, 4), (0, -5), (-3, -2)

3. 4x - 6y <= -18
(8, 5), (-9, -2), (0, 3), (5, -3)

4. 9x - 5y > 15
(5, 6), (-2, -7), (0, 4), (3, -1)

5. 7x - 4y >= 18
(4, -1), (-2, -8), (8, 9), (-3, 0)

6. Mary maneja un camión de refrescos. Su camión está cargado con latas de 15 onzas y botellas de 70 onzas. Sea **c** el número de onzas y **b** el número de botellas de 70 onzas. La carga del camión debe ser menor de 112,000 onzas. Escribir una desigualdad que describa esto.
* 9x - 5y > 15

7. En un taller de autos toma 9 minutos hacer un cambio de aceite y 15 minutos un cambio de llantas, **x** representa el número de cambios de aceite, **y** el número de cambios de llantas. Escriba una desigualdad del caso en un tiempo no mayor a 120 minutos.
* 9x + 4y > 14

8. Pablo planea correr y nadar. Sea **r** el número de vueltas que corre y **s** el número de vueltas que nada. Cada vuelta que corre tarda 3 minutos, y cada vuelta que nada toma 2 minutos. El quiere ejercitarse por lo menos 45 minutos. Escribir una desigualdad que describa el caso.
* 4x - 6y > -18

## Actividad 9

![alt text](./assets/alg33.jpg "Hover text")

2. Hoy en el almuerzo Ivanna le sirve a su hijo papitas fritas y alitas de pollo. Sea **f** el número de papitas fritas y **c** el número de alitas de pollo. Cada papita tiene **25** calorías y cada alita **100** calorías. Ivanna quiere la cantidad total de calorías de las papitas y las alitas sea menor de **700** calorías, utilizar los valores y las variables dadas para escribir una desigualdad que describa esto. `25f + 100c < 700`

3. Por su servicio telefónico Kevin para una cuota mensual de **$19** y paga **$0.06** adicionales por minuto de uso. La cantidad menor que el ha tenido que pagar por més es de **$97.72** ¿Cuáles son las posibles cantidades de minutos que utiliza su teléfono mensualmente? m = minutos. Resolver la desigualdad.
  * 19 + 0.06m >= 97.72
  * 0.06m >= 97.72 - 19
  * 0.06m >= 78.72
  * m >= 78.72 / 0.06
  * m >= 1312
