# Quimica

[//]: <> (Schoology - 58CM-CZRC-PNJVV)

![alt text](./assets/qui01.png "Hover text")
![alt text](./assets/qui02.png "Hover text")
![alt text](./assets/qui03.png "Hover text")

* Profesor: Eréndira Judith Martínez Alcántara
*	Inició de ciclo: 9 de septiembre
*	Fin de ciclo: 21 de diciembre
*	Horario de clase: Martes 7-9 y Viernes 10-12
*	Periodo de vacaciones: 23 de diciembre al 4 de enero
*	1er parcial: 8 de octubre (unidad 1 y 2)
*	2do parcial: 12 de noviembre
* Equipo `7`
* Realizar practicas `1`, `2` y `4` para el viernes 4 de octubre

| Tiene propiedades que pueden ser | Está formada por átomos | Puede convertirse en energía |
|---|---|---|
| Generales como inercia, masa, peso y volumen | Que forman moléculas | Produce cambios que pueden ser |
| Específicas como densidad, solubilidad temperatura de fusión y de ebullición | De elementos y compuestos | Físicos y químicos |

* Un fenómeno es un cambio que ocurre en la naturaleza

## Temas de estudio
*	Definición de química
*	Importancia de la química
*	Relación de la química con otras ciencias
*	Divisiones de la química
*	Conceptos de materia y energía
*	Ley de conservación de la materia
*	Ley de la conservación de la energía
*	Ley de la conservación de las masa - energía

## Definiciones
*	Ciencia es el conocimiento cierto de las cosas por sus principios y causas
*	Método científico: Observación, Hipotesis, Experimentación, Teoría y Ley
*	La química orgánica estudia los compuestos que contienen carbono
*	Química es la ciencia que estudia la materia en su estructura, composición, propiedades y cambios que experimenta y la energía involucrada en dichos cambios
*	Relación y división de la química con otras ciencias
*	Incluir páginas 6 y 7 de Zarraga con Scanner


## Propiedades generales de la materia
Las propiedades generales de la materia son: masa, inercia, peso y volumen
*	Materia: Es todo lo que ocupa un lugar en el espacio
*	Masa: Es la cantidad de materia que tiene un cuerpo
*	Peso: Es el efecto de la fuerza de gravedad sobre la masa de un cuerpo
*	Volumen: El espacio que ocupa la materia
*	Inercia: Es la propiedad que tienen los cuerpos de mantener su estado de reposo o movimiento a menos que un agente externo lo modifique

## Propiedades específicas de la materia
Las propiedades específicas son: forma, color, brillo, tamaño, olor, sabor y densidad
*	Densidad: ala relación entre la cantidad de masa que tiene un cuerpo y el volumen que ocupa
*	Densidad relativa: Es la relación entre la densidad de una sustancia y la densidad de otra, considerada como un patrón

## Leyes de la materia y energía
*	Ley de la conservación de la materia: La materia no se crea ni se destruyen solo se transforma. Antoine Lavoisier (1793-1794)
*	Ley de la conservación de la energía: Le energía del Universo no puede ser creada ni destruida, solo cambia de una forma a otra permaneciendo constante. James P. Joule (1818-1889)
*	Ley de la conservación de la masa-energía: La masa es la cantidad de energía acumulada, la energía es una manifestación de la masa. Albert Einstein (1879-1955)

## Clasificación de la materia
* Materiales
* Homogéneos | Heterogéneos
* Sustancia | Mezcla Homogénea | Mezcla Heterogénea
* Elemento | Compuesto | Solución | Mezcla
* <= Orden | Desorden =>

## Definiciones
* Elemento:
* Compuesto:
* Solución:
* Mezcla:

## Átomos y moléculas
* Demócrito introduce el término átomo como la parte más pequeña de la materia. Atomo = Sin división
* Teoría atómica de Dalton: Trataba de explicar las leyes de la época sobre la composición de las sustancias (leyes ponderales). Dalton decía que:
1.	La materia está constituida por unidades de pequeño tamaño denominadas átomos.
2.	Todos los átomos de un elemento son iguales en masa y propiedades.
3.	Los átomos de diferentes elementos son diferentes en masa y propiedades.

## Teoría atómica de Dalton (1766 - 1844)
-	Los átomos se unen entre si formando compuestos.
-	Los átomos de cada clase suelen estar en una relación constante.

## Modelo de Thomson (1897)
Se basó en su experiencia con el tubo de descarga. En el interior existe un gas sometido a una diferencia de potencial. Desde el cátodo (negativo) se emite una radiación hacia el ánodo (positivo). La radiación es emitida por el gas. Si la radiación viaja en sentido del cátodo (-) al ánodo (-), su naturaleza será negativa. Además, está formada por partículas discretas al terminar impactando en forma de chasquidos en la placa del final del tubo. Se había descubierto una partícula constitutiva de la materia: El electrón.

![alt text](./assets/qui04.jpg "Hover text")

* El modelo actual de átomo es el del Rutherford

## Modelo de Bohr
Niels Bohr (1885 – 1962) propuso un nuevo modelo atómico, a partir de los descubrimientos sobre la naturaleza de la luz y la energía. Los electrones giran en torno al núcleo en niveles energéticos bien definidos. Cada nivel puede contener un número máximo de electrones. Es un model precursor del actual.

## Descubrimiento del neutrón
Investigando las diferencias entre el número de protones y la masa del átomo, descubrió una nueva partícula: El neutrón. El neutrón posee masa similar al protón. Sin carga eléctrica. El neutrón permite explicar la estabilidad de los protones en el núcleo del átomo, manteniéndolos unidos, y por tanto justificando la no repulsión de estos en dicho núcleo, a pesar de poseer el mismo signo de carga (+).

## Modelo actual
Los electrones no describen orbitas definidas, si no que se distribuyen en una determinada zona llamada orbital. En esta región la probabilidad de encontrar al electrón es muy alta (95%). Se distribuyan en diferentes niveles energéticos en las diferentes capas.

* El descubrimiento de los neutrones no lo hizo Niels Bohr

## Número atómico y numero masico
* Numero atómico (Z): Es el número de protones que tiene los núcleos de los átomos de un elemento. Todos los átomos de un elemento tienen el mismo numero de protones. Como la carga del átomo es nula, el número de electrones será igual el numero atómico.
* Numero másico (A): Es la suma del numero de protones y de neutrones

## Isotopos
Son átomos que tienen el mismo número atómico, pero diferente número másico. Por lo tanto, la diferencia entre 2 isótopos de un elemento es el número de neutrones en el núcleo. La forma más común es el hidrógeno, que es el único átomo que no tiene neutrones en su núcleo.
* H11: Hidrogeno
* H12: Deuterio
* H13: Tritio

## Iones
Los átomos con carga eléctrica. Cuando un átomo gana electrones, adquiere un exceso de carga negativa formando un ion negativo o anión que se representa como una X-. Cuando un átomo pierde electrones tiene defecto de carga negativa. O más carga positiva que negativa. Formando un ion positivo o catión, que se representa con X+

## Distribución de los electrones en la corteza
Según el modelo fijado en nuestro trabajo, los electrones se distribuyen en diferentes niveles, que llamaremos capas. Con un número máximo de electrones en cada nivel o capa.

| Nivel | Número máximo de electrones |
|:-:|:-:|
| 1 | 2 |
| 2 | 8 |
| 3 | 18 |
| 4 | 32 |
| 5 | 32 |
| 6 | 18 |

## Distribución de los electrones en la corteza
* Así, en un elemento como el potasio en estado neutro
19K = 19 protones; 19 electrones; 20 neutrones
* 1ra capa: 2e-
* 2da capa: 8e-
* 3ra capa: 9e-

![alt text](./assets/qui05.jpg "Hover text")

### Determinar la configuración electrónica de los siguientes elementos
![alt text](./assets/qui06.jpg "Hover text")

## Distribución electrónica
Hemos visto como los átomos se distribuyen en niveles o capas de energía.
Dentro de cada nivel existen además subniveles con probabilidad de encontrarnos electrones.

## Formación de iones más probables
Un ion perderá o ganará electrones, hasta que se estabilice. La forma más común de estabilización es la de formar estructuras electrónicas de gas noble

### ¿Porqué de gas noble?
Los gases nobles son los elementos que menos tienden a perder o ganar electrones, no reaccionan apenas, solo bajo condiciones extremas. Por tanto, todos los átomos tienden a adquirir una estructura electrónica similar a la de estos.

### Configuración electrónica de 3 gases nobles
![alt text](./assets/qui07.jpg "Hover text")

Porque buscan lograr la estabilidad, como la piedra que car rodando por una montaña logra su estabilidad cuando se detiene, cada elemento de la tabla periódica loga su estabilidad cuando adquiere la estructura electrónica del gas noble.
* Quedando el último nivel de energía de cada uno de estos átomos con 8 electrones.
Excepto los átomos que se encuentran cerca del Helio, que completan su último nivel con solo 2 electrones.
* Por esta razón se denomina a ésta: REGLA DEL OCTETO

## Estados de la materia
* Estos son: sólido, líquido, gas y plasma
En física y química el plasma es un estado de la materia en el que todos los átomos están ionizados y con la presencia de una cierta cantidad de electrones libres no ligados a ningún átomo o molécula.
* El plasma en resumen es un fluido formado por electrones y iones

## Conceptos
* Sublimación: Pasa de sólido a gas y luego de gas a sólido
* Cristalización: Evaporación de un líquido donde queda un sólido (por ejemplo evaporar el agua de mar para obtener la sal)
* Cromatografía: Es un método físico de separación para la caracterización de mezclas complejas cuyo objetivo es separar los distintos componentes

## Propiedades de la materia
* Las propiedades intensivas son aquellas que no dependen de la masa o del tamaño de un cuerpo. Si el sistema se divide en varios subsistemas su valor permanecerá inalterable, por este motivo no son propiedades aditivas.
* Las propiedades extensivas son aquellas que sí dependen de la masa o del tamaño de un cuerpo, son magnitudes cuyo valor es proporcional al tamaño del sistema que describe, son propiedades aditivas.

## Cambios de estado
* Fusión: Cambio físico de sólido a líquido
* Evaporación: Cambio físico de líquido a gas
* Condensación: Cambio físico de gas a líquido
* Sublimación: Cambio físico de sólido a gas
* Solidificación: Cambio físico de líquido a sólido
* Deposición: Cambio físico de gas a sólido

## Métodos de separación de mezclas
* Filtración: Se separa un sólido de cierto tamaño de un líquido u otro sólido de diferente tamaño
* Cristalización: Separar la sal del agua dejando que el agua se evapore
* Destilación: Cuando el vapor se para por un refrigerante y recuperar el líquido
* Cromatografía: Método físico de separación para la caracterización de mezclas complejas cuyo objetivo es separar los distintos componentes

## Método unitario de factor unitario para la resolución de problemas
Todo

## Sistemas de unidades
Todo

## Teoría atómica y sistema periódico de los elementos
La teoría de Dalton solo establece que los átomos no pueden romperse por medios químicos
* Para dividir a los átomos, son necesarias técnicas de alta energía

## Estructura atómica
Las principales partículas subatómicas son electrones, protones y neutrones
* El protón y el neutrón están compuestos de quarks
Núcleos y electrones, tienen cargas eléctricas opuestas, el átomo es neutro

## Ideas
* “No hay sino átomos y espacio, todo lo demás es solamente una opinión” – Demócrito de Abdera (Griegos: átomo, sin división)
* Chadwick: Descubrió el neutrón (Nature página 312, 27 de febrero de 1932)
* Rutherford: Demostró que los átomos no eran macizos. Poseían protones dentro del núcleo. Órbitas circulares (1911)
* Bohr: Electrones alrededor del núcleo en niveles bien definidos, con un número limitado de ellos (1913)

## Modelo actual: Mecánica cuántica moderna
* De Broglie: La luz como onda y partícula
* Schrodinger: Descripción del modelo mecano - cuántico del átomo. La ecuación de onda.
* Heisenberg: Principio de incertidumbre
* Pauli: Principio de exclusión

## Conceptos importantes
* Unidad de masa atómica: Masas de los protones y neutrones en números enteros.
* 1 UMA = 1.66 * 10⁻²⁴
* Masa atómica: Número de protones + Número de electrones (peso atómico)
* Número atómico: Número de protones nucleares

## Propiedades de 3 partículas subatómicas
| Nombre | Masa en gramos | Masa en UMA | Carga eléctrica | Símbolo |
|---|---|---|---|---|
| Electrón | 9.10938 * 10²⁸ g | .00054857 UMA | 1- | e- |
| Protón | 1.67495 * 10²⁴ g | 1.00727605 UMA | 1+ | p+ o p |
| Neutrón | 1.67495 * 10²⁴ g | 1.008665 UMA | 0 | n |

* Isótopo: Átomo con el mismo número atómico, pero diferente masa atómica
* Alrededor de 250 isótopos se encuentran de forma natural. Casi 1100 han sido creados mediante el uso de reactores nucleares. Muchos isótopos sintéticos se utilizan en medicina.

## Padre de la teoría atómica-molecular: John Dalton
*	Para él tenía que cumplirse, ante todo, que los átomos de cada elemento debían tener la misma masa.

## Dalton 1808
* Los elementos están formados por partículas muy pequeñas, llamadas átomos, que son indivisibles e indestructibles
* Todos los átomos de un elemento tienen la misma masa atómica
* Los átomos se combinan en relaciones sencillas para formar compuestos
* Los átomos compuestos están formados por átomos diferentes; las propiedades del compuesto dependen del número y de la clase de átomos que tenga

## ¿Como surgió la escala química de masas atómicas?
Dalton no consiguió medir la masa absoluta de los átomos, pues sabes que es extremadamente pequeña, por lo que trató de calcular la masa de los átomos con relación el hidrógeno, al que dio el valor de unidad
* Posteriormente se tomó como átomo de referencia al oxígeno, al que se le atribuyó una masa igual a 16, y se definió la unidad de masa atómica (UMA) como 1/16 de la masa del oxígeno

## Ernest Rutherford (1871 - 1937)
Hizo investigaciones sobre la estructura atómica y sobre la radioactividad, inició el camino a los descubrimientos más notables del siglo, estudió experimentalmente la naturaleza de las radiaciones emitidas por los elementos radioactivos.
* Tras las investigaciones de Geiger y Mardsen sobre la dispersión de partículas alfa al incidir sobre láminas metálicas, se hizo necesario la revisión del modelo atómico de Thomson, que realizó Rutherford entre 1909 – 1911
Según la ya probada teoría electromagnética de Maxwell, al ser el electrón una partícula cargada en movimiento debe emitir radiación constante y por tanto perder energía. Esto debe hacer que disminuya el radio de su orbita y el electrón terminaría por caer en el núcleo; el átomo sería inestable.
* Era conocida la hipótesis de Planck que no era tenida en cuenta

## Espectro atómico (pregunta de examen)
Se llama espectro atómico de un elemento químico al resultado de descomponer una radiación electromagnética compleja en todas las radiaciones sencillas que la componen, caracterizadas cada una por un valor de longitud de onda, λ
* `Radio / Microondas / Infrarrojo / Rojo / Amarillo / Azul / Violeta / Ultravioleta / Rayos X / Rayos Gamma`

## Formula de Rydberg
* Permite calcular la longitud de onda de cualquiera de las líneas que forman el espectro del hidrogeno
La serie de Lyman corresponde a radiación ultravioleta; la seria de Balmer, a radiación visible, y el resto, a radiación infrarroja

## Tabla periódica

* Mendeleev es el padre de la tabla períodica moderna
* Tanto Mendeleev como Mayer ordenaron la tabla de manor a mayor de acuerdo a su masa atómica
* Mendeleev propuso que si el peso atómico de un elemento lo situaba en el grupo incorrecto, entonces el peso atómico debía estar mal medido. Así corrigió las masas de Be, In y U.
* Henry Moseley en 1913, mediante estudios de rayos X determinó la carga nuclear (número atómico) de los elementos. Reagrupó los elementos en orden creciente de número atómico
* Glenn Seaborg tras participar en el descubrimiento de 10 nueovs elementos, en 1944 sacó 14 elementos de la estructura principal de la Tabla Periódica, proponiendo su actual ubicación debano de la seria de los Lántanidos. El es la única persona que ha tenido un elemento que lleva su nombre en vida.

## Preguntas de exámen

* El conjunto de elementos que ocupan una línea horizontal se denomina `Periodo`
* Los elementos que conforman un mismo grupo presentan propiedades físicas y químicas similares
* Las columnas verticales se denominan `grupos` o `familias`
* Radio atómico: Es la distancia que existe desde el nucleo del átomo y el último nivel de energía

## Enlace químico (2do parcial)

* Para formar un compuesto 2 o más átomos deben reaccionar mutuamente
* En una reacción química solo los electrones de los niveles exteriores interactuan
* La regla del octeto se basa en que los átomos que tienen el su ultimo nivel 8 electrones son más estables. De manera que todos los átomos tienden a tener 8 electrones en su última orbita.


| Tipo de molecula | Ejemplo | Diferencia de electronegatividad |
|---|---|:-:|
| Molécula no polar (covalente puro) | H2 ó Cl2 | 0 |
| Molécula covalente polar | HCl | 0-5 a 1-7 |
| Compuesto ionico | NaCl |+1.7 |

* Riesgos en manejo de residuos (Dr. Francisco Javier Hormigos Ovejero - 2014)

## Enlace químico

Cuando los átomos se unen para formar grupos eléctricamente neutros, con uns consistencia tal que se pueden considerar una unidad, se dice que están formando moléculas.

* O₂ - Diatómica
* SO₂ - Triatómica
* NH₃ - Tetraatómica

## Electronegatividad

Capacidad que tiene un átomo de atraer electrones comprometidos en un enlace.

* Los valores de Electronegatividad son útiles para predecir el tipo de enlace que se puede formar entre átomosde diferentes elementos

* La electronegatividad determina el tipo de enlace que puede darse entre:
  1. Átomos iguales, en los cuales la diferencia de electronegatividad es 0 y el enlace es covalente puro o no polar, por ejemplo: H₂, Cl₂, H₂
  2. Átomos diferentes en los cuales la diferencia de electronegatividad es entre 0 y 1.7 y el enlace es covalente polar
  3. Átomos diferentes en los cuales la diferencia de electronegatividad es mayor a 1.7 y el enlace es iónico

## Características de los compuestos

### Iónicos

1. Son sólidos con punto de fusión alto (por lo general > 400°C)
2. Muchos son solubles en disolventes polares, como el agua
3. La mayoría es insoluble en disolventes no polares, como el hexano C₆H₁₄
4. Los compuestos fundidos conducen bien la electricidad porque contienen partículas móviles con carga (iones)
5. Las soluciones acuosas conducen bien la electricidad porque contienen partículas móviles con carga (iones)

### Covalentes

1. Son gases, líquidos o sólidos con punto de fusión bajo (por lo general < 300°C)
2. Muchos de ellos son insolubles en disolventes polares
3. La mayoría es soluble en disolventes no polares, como el hexano C₆H₁₄
4. Los compuestos líquidos o fundidos no conducen electricidad
5. Las soluciones acuosas suelen ser malas conductoras de la electricidad porque no contienen partículas con carga

## Enlace covalente

* Las reacciones entre 2 no metales producen un enlace covalente
* El enlace covalente se forma cuando 2 átomos comparten uno o más pares de electrones
* Veamos un ejemplo simple de un enlacce covalente, la reacción de 2 átomos de H para producir una molécula de H₂

### Clasificación de los enlaces covalentes

Según el número de electrones que participan en cada enlace, estos son:
* Enlace simple: 2 electrones
* Enlace doble: 4 electrones
* Enlace triple: 6 electrones

Enlace σ (sigma): Es el enlace ubicado en la región intermolecular

### Enlace covalente coordinado

Un enlace covalente coordinado es un enlace formado cuando ambos electrones del enlace son donados por uno de los átomos. Consiste en la compartición de un par de electrones proveniente del mismo átomo

## Regla del octeto

Es habitual que los elementos representativos alcancen las configuraciones de los gases nobles. Este enunciado a menudo se denomina la regla del octeto porque las configuraciones electrónicas de los gases nobles tienen 8 electrones en su capa más externa a excepción del He que tiene 2 electrones

## Regla del dueto

Así como los elementos electronegativos cumplen la regla del octeto, para alcanzar la configuración de un gas noble. El Hidrógeno cumple la regla del dueto.

* La regla del dueto consiste en que el H₂, al combinarse con otro elemento, ya sea en un enlace iónico o un enlace covalente, lo hace para completar su orbital con 2 electrones

## Estrucuras de Lewis

* La idea del enlace covalente fue sugerida por Lewis

## Nomenclatura de compuestos inorgánicos

### Fomulación y nomenclatura en química inorgánica

* La fórmula química expresa la composición de moléculas y compuestos mediante símbolos químicos.
* El número de compuestos químicos conocidos en superior a 13 millones
* Método sistemático de nombrar los compuestos: Nomenclatura

* Unión Internacional de Química Pura y Aplicada (International Union of Pure and Applied Chemistry, IUPAC)

### Sistemas de nomenclaturas

* Sistemática (propuesta por la IUPAC)
* Stock
* Tradicional (el sistema más antiguo)

### Sustancia simples

Se llaman sustancias simples a aquellas que están constituidas por átomos de un solo elemento

| Compuesto | Sistemática (IUPAC) | Tradicional |
|:-:|:-:|:-:|
| H₂ | Dihidrógeno | Hidrógeno |
| F₂ | Diflúor | Flúor |
| Cl₂ | Dicloro | Cloro |
| Br₂ | Dibromo | Bromo |
| I₂ | Diyodo | Yodo |
| O₂ | Dioxígeno | Oxígeno |
| O₃ | Trioxígeno | Ozono |
| S₈ | Octaazufre | Azufre |
| P₄ | Tetrafósforo | Fósforo |

* Los gases nobles son monoatómicos: `He, Ne, Ar, Kr, Xe, Rn`
* Los metáles se representan simplemente mediante el símbolo: `Cu, Sn, Fe, Ag`

## Número de oxidación

* El número de oxidación de un elemento viene a ser equivalente a su capacidad de combinación con un signo positivo o negativo. En la tabla siguiente se indican los estados de oxidación formales más usuales:

![alt text](./assets/qui08.jpg "Hover text")

## Compuestos binarios

Los compuestos binarios están formados por 2 elementos

* Se escriben los elementos en un orden: Primero el menos electronegativo y luego el más electronegativo
* Se intercambian los números de oxidación, pero prescindiendo del signo: `Al⁺³₂ O⁻²₃`
* Siempre que es posible se simplifica: `Cu⁺²₂ S⁻²₂ => CuS`
* El compuesto se lee de `derecha a izquierda`

### Nomenclarutra Sistemática

* Consiste en la utilización de prefijos numerales griegos para inndicar el número de átomos de cada elemento presenta en la fórmula
* Los prefijos que se utilizan son:
  1. mono
  2. di
  3. tri
  4. tetra
  5. penta
  6. hexa
  7. hepta
* El prefijo mono puede omitirse

| Formula | Nombre |
|:-:|:--|
| Cl₂O₅ | Pentaóxido de dicloro |
| H₂S | Sulfuro de dihidrógeno |
| SiH₄ | Tetrahidruro de silicio |

### Nomenclatura Stock

Consiste en indicar el número de oxidación con números romanos y entre paréntesis, al final del nombre del elemento. Si éste tiene número de oxidación único, no se indica.

* n.o. = Número de oxidación

| Formula | Nombre |
|:-:|:--|
| CuO | Óxido de cobre (II) |
| Fe₂O₃ | Óxido de hierro (III) |
| Al₂O₃ | Óxido de aluminio |

### Nomenclatura tradicional

Consiste en añadir un sufijo al nombre del elemento según con el número de oxidación con el que actúe

| Posibilidad de n.o. | Terminación |
|:-:|---|
| uno | -ico |
| dos | n.o. menor => -oso |
| dos | n.o. mayor => -ico |
| tres | n.o. menor => hipo- ... -oso |
| tres | n.o. intermedio => -oso |
| tres | n.o. mayor => -ico |
| cuatro | n.o. menor => hipo- ... -oso |
| cuatro | n.o. intermedio => -oso |
| cuatro | n.o. intermedio => -ico |
| cuatro | n.o. mayor => per- ... -ico |

### Comparaciones

| Formula | Stock | IUPAC | Tradicional |
|:-:|:--|:--|:--|
| Al₂O₃ | Óxido de aluminio | Trióxido de dialuminio | Óxido Aluminico |
| Fe₂O₂ = FeO | Óxido de fierro (II) | Óxido de fierro | Óxido Ferroso |
| Fe₂O₃ | Óxido de fierro (III) | Trióxido de difierro | Óxido Ferrico |

### Clasificación

* Óxidos
  * Óxidos básicos
  * Óxidos ácidos
* Peróxidos
* Hidruros
  * Hidruros metálicos
  * Hidruros no metálicos
    * Hidruros volátiles (grupos 13, 14 y 15)
    * Haluros de hidrógeno (grupos 16 y 17)
* Sales neutras
  * Sales neutras
  * Sales volátiles

## Compuestos binarios óxidos

* Un óxido es una combinaciónn del oxígeno con cualquier elemento químico
* Valencia = Número de oxidación

Óxido básico: Es la combinación de un oxígeno con un metal

| Compuesto | Sistemática | Stock | Tradicional |
|:-:|:-:|:-:|:-:|
| FeO | Monóxido de hierro | Óxido de hierro (II) | Óxido ferroso |
| Fe₂O₃ | Trióxido de dihierro | Óxido de hierro (III) | Óxido férrico |
| Li₂O | Óxido de dilitio | Óxido de litio | Óxido lítico o de litio |

![alt text](./assets/qui09.jpg "Hover text")

Óxido ácido: Es la combinación del oxígeno con un no metal

* Los óxidos ácidos también son conocidos como anhídridos

| Compuesto | Sistemática | Stock | Tradicional | Exponentes | Subindices | Simplificación | Resultado final |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| SO | Monóxido de azufre | Óxido de azufre (II) | Anhídrido hiposulfoso | S⁺²O⁻² | S₂O₂ | S₁O₁ | SO |
| SO₂ | Dióxido de azufre | Óxido de azufre (IV) | Anhídrido sulfuroso | S⁺⁴O⁻² | S₂O₄ | S₁0₂ | SO₂ |
| SO₃ | Trióxido de azufre | Óxido de azufre (VI) | Anhídrido sulfúrico | S⁺⁶O⁻² | S₂O₆ | S₁0₃ | SO₃ |
| CO | Monóxido de azufre | Óxido de carbono (II) | Anhídrido carbonoso | C⁺²O⁻² | C₂O₂ | C₁O₁ | CO |
| CO₂ | Dióxido de azufre | Óxido de carbono (IV) | Anhídrido carbónico | C⁺⁴O⁻² | C₂O₄ | C₁0₂ | CO₂ |

Hidruro: Es la combinación del hidrógeno con cualquier elemento químico

* Hidruros metálicos: Son la combinación del hidrógeno (-1) con un metal

| Compuesto | Sistemática | Stock | Tradicional |
|:-:|:-:|:-:|:-:|
| CaH₂ | Dihidruro de calcio | Hidruro de calcio | Hidruro cálcico |
| LiH | Hidruro de litio | Hidruro de litio | Hidruro lítico |
| FeH₃ | Trihidruro de hierro | Hidruro de hierro (III) | Hidruro férrico |
| SrH₂ | Dihidruro de estroncio | Hidruro de estroncio | Hidruro de estroncio |

* Hidruros no metálicos: Es la combinación del hidrógeno (+1) con un no metal de los grupos VIA Y VIIA, también se les llama: Haluros de hidrógeno

| Compuesto | Sistemática | Stock | Tradicional |
|:-:|:-:|:-:|:-:|
| HF | Fluoruro de hidrógeno | Fluoruro de hidrógeno | Ácido fluorhídrico |
| HCl | Cloruro de hidrógeno | Cloruro de hidrógeno | Ácido clorhídrico |
| H₂S | Sulfuro de hidrógeno | Sulfuro de hidrógeno | Ácido sulfhídrico |
| H₂Se | Seleniuro de hidrógeno | Seleniuro de hidrógeno | Ácido selenhídrico |


* Hidrúros volátiles (hidruros no metálicos): Es la combinación del hidrógeno (+1) con un no metal de los grupos IIIA, IVA y VA

| Compuesto | Sistemática | Stock | Tradicional |
|:-:|:-:|:-:|:-:|
| HN₃ | Trihidruro de nitrógeno | Hidruro de nitrógeno (III) | Amoniaco |
| PH₃ | Trihidruro de fósforo | Hidruro de fósforo (III) | Fosfina |
| AsH₃ | Trihidruro de arsénico | Hidruro de arsénico (III) | Arsina |
| SbH₃ | Trihidruro de antimonio | Hidruro de antimonio (III) | Estibina |
| CH₄ | Trihidruro de carbono | Hidruro de carbono (III) | Metano |
| SiH₄ | Trihidruro de silicio | Hidruro de silicio (III) | Silano |
| BH₃ | Trihidruro de boro | Hidruro de boro (III) | Borano |

* Sales neutras: Son combinaciones de 2 elementos, que no son ni el `O`, ni el `H`. Son combinaciones de un metal y un no metal

| Compuesto | Sistemática | Stock | Tradicional |
|:-:|:-:|:-:|:-:|
| LiF | Floruro de litio | Floruro de litio | Floruro de lítico |
| AuBr₃ | Tribromuro de oro | Bromuro de oro (III) | Bromuro áurico |
| Na₂S | Sulfuro de disodio | Sulfuro de sodio | Sulfuro sódico |
| SnS2 | Disulfuro de estaño | Sulfuro de estaño (IV) | Sulfuro estánnico |

* Sales volátiles: Son combinaciones de 2 no metales. Se escribe a la izquierda el elemento que se encuentre primero en esta relación:

```
B < Si < C < Sb < As < P < N < Te < Se < S < I < Br < Cl < O < F
```

Se recomienda utilizar la nomenclatura sistemática

| Compuesto | Sistemática | Stock | Tradicional |
|:-:|:-:|:-:|:-:|
| BrF₃ | Trifloruro de bromo | Floruro de bromo (III) | - |
| BrCl | Cloruro de bromo | Cloruro de bromo (I) | - |
| CCl₄ | Tetracloruro de carbono | Cloruro de carbono (IV) | - |
| As₂Se₃ | Triseleniuro de diarsénico | Seleniuro de arsénico (III) | - |

## Combinaciones

| - | NO₂⁻ | SO₄⁻² | S⁻² | PO₄⁻³ | MnO₄⁻ | SCN⁻ |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Na⁻ | Na(NO)₂ <br> Nitrito de sodio | Na₂(SO₄) <br> Sulfato de sodio | Na₂(S) <br> Sulfuro de sodio | Na₃(PO₄) <br> Fosfato de sodio | Na(MnO₄) <br> Permanganato de sodio | Na(SCN) <br> Triocianato de sodio |
| K⁺ | K(NO)₂ <br> Nitrito de potasio | K₂(SO₄) <br> Sulfato de potasio | K₂(S) <br> Sulfuro de potasio | K₃(PO₄) <br> Fosfato de potasio | K(MnO₄) <br> Permanganato de potasio | K(SCN) <br> Triocianato de potasio |
| Ca⁺² | Ca(NO₂)₂ <br> Dinitrito de calcio | Ca₂(SO₄)₂ <br> Disulfato de calcio | Ca(S) <br> Sulfuro de calcio | Ca₃(PO₄)₂ <br> Difosfato de calcio | Ca(MnO₄)₂ <br> Permanganato de calcio | Ca(SCN)₂ <br> Triocianato de calcio |
| Ba⁺² | Ba(NO₂)₂ <br> Dinitrito de bario | Ba₂(SO₄)₂ <br> Disulfato de bario | Ba(S) <br> Sulfuro de bario | Ba₃(PO₄)₂ <br> Difosfato de bario | Ba(MnO₄)₂ <br> Permanganato de bario | Ba(SCN)₂ <br> Triocianato de bario |
| Al⁺³ | Al(NO₂)₃ <br> Trinitrito de aluminio | Al₂(SO₄)₃ <br> Trisulfato de aluminio | Al₂(S)₃ <br> Trisulfuro de aluminio | Al(PO₄) <br> Fosfato de aluminio | Al(MnO₄)₃ <br> Permanganato de aluminio | Al(SCN)₃ <br> Triocianato de aluminio |

| - | NO₂⁻ | SO₄⁻² | S⁻² | PO₄⁻³ | MnO₄⁻ | SCN⁻ |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Rb⁻ | Rb(NO)₂ <br> Nitrito de rubidio | Rb₂(SO₄) <br> Sulfato de rubidio | Rb₂(S) <br> Sulfuro de rubidio | Rb₃(PO₄) <br> Fosfato de rubidio | Rb(MnO₄) <br> Permanganato de rubidio | Rb(SCN) <br> Triocianato de rubidio |
| Cs⁺ | Cs(NO)₂ <br> Nitrito de cesio | Cs₂(SO₄) <br> Sulfato de cesio | Cs₂(S) <br> Sulfuro de cesio | Cs₃(PO₄) <br> Fosfato de cesio | Cs(MnO₄) <br> Permanganato de cesio | Cs(SCN) <br> Triocianato de cesio |
| Cr⁺² | Cr(NO₂)₂ <br> Dinitrito de cromo | Cr₂(SO₄)₂ <br> Disulfato de cromo | Cr(S) <br> Sulfuro de cromo | Cr₃(PO₄)₂ <br> Difosfato de cromo | Cr(MnO₄)₂ <br> Permanganato de cromo | Cr(SCN)₂ <br> Triocianato de cromo |
| W⁺² | W(NO₂)₂ <br> Dinitrito de wolframio | W₂(SO₄)₂ <br> Disulfato de wolframio | W(S) <br> Sulfuro de wolframio | W₃(PO₄)₂ <br> Difosfato de wolframio | W(MnO₄)₂ <br> Permanganato de wolframio | W(SCN)₂ <br> Triocianato de wolframio |
| Ga⁺³ | Ga(NO₂)₃ <br> Trinitrito de galio | Ga₂(SO₄)₃ <br> Trisulfato de galio | Ga₂(S)₃ <br> Trisulfuro de galio | Ga(PO₄) <br> Fosfato de galio | Ga(MnO₄)₃ <br> Permanganato de galio | Ga(SCN)₃ <br> Triocianato de galio |



## Radicales

| Nombre | Formulas o simbolo | Número de oxidación |
|:-:|:-:|:-:|
| Hidruro | H | -1 |
| Floruro | F | -1 |
| Cloruro | Cl | -1 |
| Yoduro | I | -1 |
| Bromuro | Br | -1 |
| Cianuro | CN | -1 |
| Hidróxido | OH | -1 |
| Hipoclorito | ClO | -1 |
| Clorito | ClO₂ | -1 |
| Clorato | ClO₃ | -1 |
| Perclorato | ClO₄ | -1 |
| Hipobromito | BrO | -1 |
| Bromito | BrO₂ | -1 |
| Perbromato | BrO₄ | -1 |
| Yodato | IO₃ | -1 |
| Peryodato | IO₄ | -1 |
| Nitrito | NO₂ | -1 |
| Nitrato | NO₃ | -1 |
| Permanganato | MnO₄ | -1 |
| Triosianato | SCN | -1 |
| Bicarbonato | HCO₃ | -1 |
| Bisulfato | HSO₄ | -1 |
| Bisulfito | HSO₃ | -1 |
| Peróxido | O₂ | -2 |
| Oxido | O | -2 |
| Sulfuro | S | -2 |
| Sulfato | SO₄ | -2 |
| Sulfito | SO₃ | -2 |
| Trisulfato | S₂O₃ | -2 |
| Carbonato | CO₃ | -2 |
| Cromato | CrO₄ | -2 |
| Dicromato | Cr₂O₇ | -2 |
| Oxalato | C₂O₄ | -2 |
| Seleniuro | Se | -2 |
| Teluro | Te | -2 |
| Nitruro | N | -3 |
| Fosfuro | P | -3 |
| Fosfito | PO₃ | -3 |
| Fosfato | PO₄ | -3 |
| Arsenito | AsO₃ | -3 |
| Arseniato | AsO₄ | -3 |

## Química orgánica

Estudia las estructuras, propiedades y síntesis de los compuestos orgánicos. El carbono es el elemento común a éstos.

* Propiedades de los compuestos orgánicos e inorgánicos

| Propiedad | Compuesto orgánico | Compuesto inorgánico |
|:-:|:-:|:-:|
| Puntos de fusión | Bajos | Altos |
| Puntos de ebullición | Bajos | Altos |
| Solubilidad el agua | Baja | Gran solubilidad |
| Solubilidad el solventes no polares | Alta | Baja |
| Inflamabilidad | Inflamables | No inflamables |
| Conducción de electricidad | Sus soluciones no conducen la electricidad | Sus soluciones conducen la electricidad |
| Reacciones químicas | Son lentas | Son rápidas |
| Isomería | Exiben isomería | En pocos casos |
| Tipo de enlace | Covalente | Iónico |
| Estado físico a temperatura ambiente | Gas, líquido y sólido | Predomina el sólido |

### Características generales del carbono y su estructura

* Masa atómica: `12`
* Número atómico: `3`
* Configuración electrónica: `1S² 2S² 2P²`
* Electrones de valencia: `4`
* Puede adquirir 4 electrones para llenar su capa externa
* Tetravalente
* Capacidad de enlazarse sucesivamente a otros átomos de carbono para formar cadenas o anillos

### Isomería

Los isómeros son compuestos que tienen la mismo fórmula molecular pero diferente fórmula estructural

* C₂H₆O
* CH₃CH₂OH
* CH-O-CH₃

Se clasifica en:
  * Estereoisomería
    * Óptica
    * Conformación
    * Geométrica
  * Estructural
    * Funcional
    * Posición
    * Esqueleto

* Isomeros estrucurales: Son los que varían en la uniones de los átomos a través de enlaces
* Isomeros de esqueleto: Son los compuestos que difieren en la disposición de la cadena de carbono
* Isomeros de posición: Son los que difieren en la ubicación de un grupo que no tiene carbono o un doble o triple enlace

* Química orgánica === Química del carbono

## Clasificación de los hidrocarburos

* Los hidrocarburos son los compuestos orgánicos más sencillos, y solo contienen átomos de carbono e hidrógeno

Se clasifican como:
* Alifáticos
* Aromáticos
  * Saturados
    * Alcanos
  * Insaturados
    * Alquenos
    * Alquinos

## Nomenclatura de hidrocarburos de cadena lineal

* Son aquellos que constan de un _prefijo_ que indica el número de átomos de carbono, y de un _sufijo_ que revela el tipo de hidrocarburo

* Los sufijos empleados para los alcanos, alquenos y alquinos son respectivamente: **-ano**, **-eno** e **-ino**

| Prefijo | N° de átomos de C |
|:-:|:-:|
| Met | 1 |
| Et | 2 |
| Prop | 3 |
| But | 4 |
| Pent | 5 |
| Hex | 6 |
| Hept | 7 |
| Oct | 8 |
| Non | 9 |
| Dec | 10 |
| Undec | 11 |
| Dodec | 12 |
| Tridec | 13 |
| Tetradec | 14 |
| Eicos | 20 |
| Triacont | 30 |

![alt text](./assets/qui10.jpg "Hover text")

## Alquenos

Los alquenos son hidrocarburos con dobles enlaces

* La posición del doble enlace, se indica con un localizador, empezando a numerar la cadena por el extremo más próximo al doble enlace
* El localizador es el número correspondiente al primer carbono del doble enlace y se escribe delante del nombre separado por un guión
* Se nombran sustituyendo la terminación **-ano**, por **-eno**
* Si el alqueno tiene 2 o más dobles enlaces, numeramos la cadena asignando a los dobles, los localizadores más bajos
* Se utilizan las terminaciones **-dieno**, **-trieno**

![alt text](./assets/qui11.jpg "Hover text")

## Alquinos

Los alquinos son hidrocarburos con triples enlaces

* La nomenclatura de los alquinos se rige por reglas análogas a las de los alquenos. Solo hay que cambiar al sufijo **-eno**, por **-ino**

![alt text](./assets/qui12.jpg "Hover text")

## La ecuación química

En una reacción química, las fórmulas de los reactivos se escriben a la izquierda y separadas por una flecha de las fórmulas de los productos, que se escriben a la derecha.

* Reactivos => Compuestos
