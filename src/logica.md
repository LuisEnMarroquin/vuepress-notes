# Lógica de programación

* Profesora: Marianela Villagómez Torres

1. q <= p
* si p está en q

| q | p | q <= p |
|---|---|---|
| 0 | 0 | 1 |
| 0 | 1 | 0 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

2. r v q
* r o q

| r | q | r v q |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

3. s ^ t
* s y t

| s | t | s ^ t |
|---|---|---|
| 0 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

4. t => v
* si t esta en v

| t | v | t => v |
|---|---|---|
| 0 | 0 | 1 |
| 0 | 1 | 1 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

## Condicionales

| Conector | ¿Que es? | Valor de verdad | Condición |
|:-:|---|:-:|---|
| <-> | Bicondicional (si y solo si) | V | Si ambos tienen igual valor de verdad |
| 🔺 | Or exclusivo (disyunción fuerte) | V | Si tienen valores diferentes de verdad |
| -> | Condicional (entonces) | F | Si el antecedente es verdadero y el consecuente es falso |
| v | Disyunción 'o' (disyunción debil) | F | Si ambos son falsos |
| ^ | Conjunción 'y' | V | Si ambos son verdaderos |
| ~ | Negación 'no' | V | Si la proposición es falsa |


* El pavo será para mañana antes de las 12 de la noche: `p ^ q`
  * p = El pavo será para mañana
  * El pavo es antes de las 12 de la noche
* Luis es buen jugador o es afortunado: `p v q`
  * p = Luis en buen jugador
  * q = Luis es afortunado
* Hoy está lloviendo: `p`
  * p = Hoy llueve
* Mi perro es bonito pero huele feo: `p ^ q`
  * p = Mi perro es bonito
  * q = Mi perro huele feo
* Perú se encuentra al lado izquiero de Brazil y Brazil se encuentra en America: `p ^ q`
  * p = Perú se encuentra del lado izquiero de Brazil
  * q = Brazil se encuentra en America
* Mi gato no quiere comer: `~ p`
  * p = gato no quiere comer
* Escribo con lapicero si y solo si tiene tinta: `p <-> q`
  * p = Escribo con lapicero
  * q = Lapicero tiene tinta

### Actividad 30 septiembre

* No es cierto que no me gusta bailar: `~ (~ p)`
  * p = Es cierto que me gusta bailar
* Me gusta bailar y leer libros de ciencia ficción: `p ^ q`
  * p = Me gusta bailar
  * q = Me gusta leer libros de ciencia ficción
* Si los gatos de mi hermana no soltaran tanto pelo entonces me gustaría acariciarlos: `~ p -> q`
  * p = Los gatos de mi hermana sueltan pelo
  * q = Me gusta acariciarlos
* Si y solo si viera a un marciano con mis propios ojos, entonces creería que hay vida extraterrestre: `<-> p -> q`
  * p = Ver un marciano con mis propios ojos
  * q = Creer que hay vida extraterrestre

### Ejemplos 2 de octubre

| Signo | ¿Que es? | Significado | Codigo |
|:-:|:--|:--|:-:|
| ^ | Conjunción | Y | and |
| v | Disyunción | O | or |
| -> | Condicional | Si...entonces | if |
| <-> | Bicondicional | Si y solo si | if |
| 🔺 | Uno u otro | No ambos | - |

* No vi la pelicula, pero leí la novela: `~ p ^ q`
  * p = Vi la pelicula
  * q = Leí la novela

| p | q | ~ p | ^ | q |
|:-:|:-:|:-:|:-:|:-:|
| 0 | 0 | 1 | 0 | 0 |
| 0 | 1 | 1 | 1 | 1 |
| 1 | 0 | 0 | 0 | 0 |
| 1 | 1 | 0 | 0 | 1 |

* Ni vi la pelicula, ni leí la novela: `~ p ^ ~ q`
  * p = Vi la pelicula
  * q = Leí la novela

| p | q | ~ p | ^ | ~ q |
|:-:|:-:|:-:|:-:|:-:|
| 0 | 0 | 1 | 1 | 1 |
| 0 | 1 | 1 | 0 | 0 |
| 1 | 0 | 0 | 0 | 1 |
| 1 | 1 | 0 | 0 | 0 |

* No es cierto que viese la película y leyese la novela: `~ (r ^ s)` (`Ninguna` o `una u otra`, pero `no ambas`)
  * r = Vi la pelicula
  * s = Leí la novela

| r | s | r | ^ | s | ~ (r ^ s) |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 0 | 0 | 0 | 0 | 0 | 1 |
| 0 | 1 | 0 | 0 | 1 | 1 |
| 1 | 0 | 1 | 0 | 0 | 1 |
| 1 | 1 | 1 | 1 | 1 | 0 |

* O tu estas equivocado o es falsa la noticia que has leido: `p 🔺 q` (Disyunción fuerte `una u otra`)
  * p = Tu estas equivocado
  * q = Es falsa la noticia que has leido

| p | q | p 🔺 q |
|:-:|:-:|:-:|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |

* Si no estuvieras loca, no habrías venido aquí: `~ r -> ~ c`
  * r = Estuvieras loca
  * c = Habrías venido aquí

| r | c | ~ r | -> | ~ c |
|:-:|:-:|:-:|:-:|:-:|
| 0 | 0 | 1 | 1 | 1 |
| 0 | 1 | 1 | 0 | 0 |
| 1 | 0 | 0 | 1 | 1 |
| 1 | 1 | 0 | 1 | 0 |

* Llueve y o bien nieva o sopla el viento: `p ^ (q 🔺 z)`
  * p = Llueve
  * q = Bien nieva
  * z = Sopla el viento

| p | q | z | p | ^ | (q | 🔺 | z) |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 0 | 0 | 0 | 1 | 1 |
| 0 | 1 | 0 | 0 | 0 | 1 | 1 | 0 |
| 0 | 1 | 1 | 0 | 0 | 1 | 0 | 1 |
| 1 | 0 | 0 | 1 | 0 | 0 | 0 | 0 |
| 1 | 0 | 1 | 1 | 1 | 0 | 1 | 1 |
| 1 | 1 | 0 | 1 | 1 | 1 | 1 | 0 |
| 1 | 1 | 1 | 1 | 0 | 1 | 0 | 1 |

* Si acepto este trabajo o dejo de pintar por falta de tiempo, entonces no realizaré mis sueños: `(p v q) -> ~ r`
  * p = Acepto este trabajo
  * q = Dejo de pintar por falta de tiempo
  * r = Realizaré mis sueños

| p | q | r | (p | v | q) | -> (evaluando `v` y `~r`) | ~ r |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| 0 | 0 | 0 | 0 | 0 | 0 | 1 | 1 |
| 0 | 0 | 1 | 0 | 0 | 0 | 1 | 0 |
| 0 | 1 | 0 | 0 | 1 | 1 | 1 | 1 |
| 0 | 1 | 1 | 0 | 1 | 1 | 0 | 0 |
| 1 | 0 | 0 | 1 | 1 | 0 | 1 | 1 |
| 1 | 0 | 1 | 1 | 1 | 0 | 0 | 0 |
| 1 | 1 | 0 | 1 | 1 | 1 | 1 | 1 |
| 1 | 1 | 1 | 1 | 1 | 1 | 0 | 0 |
