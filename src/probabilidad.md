# Probabilidad y estadística

* Profesor: José Ángel Cano Gonzalez

## Conceptos básicos
*	Población: Se refiere a todos los elementos que han sido escogidos para el estudio. Puede ser finita o infinita.
*	Muestra: Describe a una porción escogida de la población para su estudio.
*	Censo: Cuando es posible examinar a cada persona o elemento de la población que deseamos describir.
*	Muestreo: Cuando no es posible contar o medir todos los elementos de la población.

## Estadísticas y parámetros
Matemáticamente, es posible describir las muestras y poblaciones al emplear algunas mediciones, como son:
*	Media
*	Mediana
*	Moda
*	Desviación estándar
1. Cuando estos términos describen las características de una muestra, se denominan “estadísticas”
2. Cuando describen las características de la población, se llaman “parámetros”

## Tipos de muestreo
*	Aleatorio o de probabilidad: Todos los elementos de la población tienen la oportunidad de ser escogidos para la muestra.
*	No aleatorio o de juicio: Se emplea el conocimiento y opinión personal, para identificar a los elementos de la población que deben incluirse dentro de la muestra.
*	Muestras sesgadas: Muestreo de personas con fuerte interés en el asunto de estudio, pudiendo repercutir en los resultados del estudio.
*	Muestreo con reemplazo: El elemento muestreado vuelve a la población después de ser elegido, e inmediatamente antes de elegir al siguiente elemento.

## Muestreo aleatorio
Selecciona muestras mediante métodos que permiten que cada posible muestra, tenga una igual probabilidad de ser seleccionada, y que cada elemento de la población total, tenga una oportunidad igual de ser incluido en la muestra. La forma más fácil de seleccionar la muestra, es mediante números generados aleatoriamente (computadora o tabla de números aleatorios)
* Ejemplo: Suponer que tenemos 100 empleados en una compañia y se desa entrevistar a 10 de ellos al azar

## Muestreo sistemático
Los elementos son seleccionados de la población, dentro de un intervalo uniforme (tiempo, orden, espacio). Cada elemento tiene igual oportunidad de ser seleccionado para cada muestra no tiene una posibilidad igual de ser seleccionada.
* Se requiere menos tiempo y costo para seleccionar la muestra.
* Existe la posibilidad de introducir un error en el proceso de muestreo.
* Ejemplo: Muestro de los desechos de los hogares después de un fin de semana.

## Muestreo estratificado
Dividir la población en grupos relativamente homogéneos (estratos)
* Seleccionar aleatoriamente un número específico de elementos por estrato, en proporción a su tamaño.
Se gatantiza que cada elemento de la población tenga posibilidad de ser seleccionado.
Si se diseñan adecuadamente, reflejan características más precisas de la población de donde provienen.
* Ejemplo: Un médico desea averiguar cuantas horas duermen sus pacientes.

## Muestro de racimo
* Dividir la población en grupos (racimos)
Seleccionar una muestra aleatoria de estos racimos y considerar a todos los elementos de cada racimo seleccionado. Puede producir muestras más precisas.
* Ejemplo: Se desea determinar por muestreo el número de televisiones por hogar en una ciudad grande.

## Muestreo estratificado
Cada grupo tiene pequeñas variaciones dentro de sí mismo. Existe una amplia variación de un grupo a otro

## Muestreo de racimo
Existe una variación considerable dentro del mismo grupo. Los diferentes grupos son muy similares

## Estratificado o de racimo
* Ejercicio ¿Que método es el más adecuado, el muestreo estratificado o el muestreo de racimo?
1. Un estudio del Senado sobre el susto de la autonomía de cierta ciudad implicó entrevistar a 2000 personas de la población de la ciudad respecto a su opinión sobre varios aspectos relacionados en esa autonomía. Es más adecuado el muestreo de racimo.
2. Se considera que en esta ciudad existen muchas secciones son pobres y muchas ricas, con muy pocas intermedias. Es más adecuado el muestreo estratificado (porque hay mucha diferencia entre una colonia y otra)
3. Los investigadores que llevaron a cabo el sondeo tenían razones para creer que las opiniones expresadas en las diferentes respuestas dependías fuertemente del ingreso. Es más adecuado el muestreo de racimo.

## Medidas de tendencia central
Se refiere al punto medio de una distribución. También se les conoce como medidas de posición.

## Dispersión
Se refiere a la separación de los datos en una distribución.

## Sesgo
Las curvas quebrepresentan los datos, pueden ser simétricas o sesgadas. En una curva sesgada, los valores no están igualmente distribuidos.

![alt text](./assets/prb01.jpg "Hover text")

* La curva A está sesgada a la derecha (positivamente sesgada)
* La curva B es completamente opuesta (negativamente sesgada)

## Curtosis
Se refiere a que tan puntiaguda es una distribución (que tan grande es la media)

![alt text](./assets/prb02.jpg "Hover text")

Ambas presentan características similares (posición central, dispersión simetría), excepto por el pico. Se dice que tienen diferentes grados de curtosis

## Media aritmetica
Casi siempre, cuando nos referimos al “promedio” de algo, estamos hablando de la media arimétrica.

![alt text](./assets/prb03.jpg "Hover text")

- M = Media arimetica
- X = valores de muestra (observaciones)
- N = tamaño de la muestra

* Ejemplo: La table presenta datos que describen el numero de dias que los generadoers de una planta de energia se encuentran fuera de servicio debido a mantenimiento normal o por alguna falta.

| Generador | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
|---|---|---|---|---|---|---|---|---|---|---|
| Dias fuera de servicio | 7 | 23 | 4 | 8 | 2 | 12 | 6 | 13 | 9 | 4 |

Media arimetrica:

![alt text](./assets/prb04.jpg "Hover text")

# Simbología estadísticos y parámetros

| Tipo de medida | Estadísticos | Parámetros |
|---|---|---|
| Numero de elementos	| n	| N |
| Media aritmetrica	| x	| μ |

Calculo de media a partir de datos no agrupados:
*	Para calcular esta media, se sumaron odas las observaciones.
*	Este dipo de datos se conoce como “No agrupados”
*	No son cálculos difíciles, al tratarse de tamaños de muestra pequeños.

Calculo de media de datos agrupados:
*	Calcular el punto medio de cada clase (se requieren cantidades cerradas)
*	Multiplicar cada medio por la frecuencia de observaciones de dicha clase.
*	Sumar resultados y dividirlos entre el número de observaciones totales.

Medidas de tendencia central:

![alt text](./assets/prb05.jpg "Hover text")

* x̄ = Media aritmetica
* f = Frecuencia de clase
* x = Valor medio de clase = Media Clase = (Limite inferior + Limite superior) / 2
* m = numero de observaciones

## La mediana
Es un solo valor del conjunto de datos, que mide la observación central del conjunto. Es el elemento que está mas al centro del conjunto de números.

## Calculo de datos no agrupados
Primero se organizan los datos, en orden de descendente o ascendente. Si el conjunto contiene un número impar de elementos el del en medio es la mediana. Si se tiene un número par de elementos, la mediana es el promedio de los 2 elementos de en medio.

## Calculo de datos agrupados
* Para determinar que observación de la distribución está más al centro.
* Sumar las frecuencias de cada clase hasta encontrar la que contiene la mediana.
* Determinar que número de elemento de la clase contiene a la mediana.
* Determinar el ancho de pasos entre el límite inferior y superior.
* Calcular el valor de la mediana multiplicando el número de elemento por el ancho de pasos.
* Si existe número par de elementos

## Ejercicio
Para la siguiente distribución de frecuencias determine:

| Clase | Frecuencia | Total |
|---|---|---|
| 100 – 149.5 | 12 | 12 |
| 150 – 199.5 | 14 | 26 |
| 200 – 249.5 | 27 | 53 |
| 250 – 299.5 | 58 | 111 |
| 300 – 349.5 | 72 | 183 |
| 350 – 399.5 | 63 | - |
| 400 – 449.5 | 36 | - |
| 450 – 499.5 | 18 | - |
| Total | 300 | - |

1. La clase de la mediana (clase modal)
* La clase 300 – 349.5 = 72

2. El número de elemento que representa la mediana
* (n + 1) / 2 = (300 + 1) / 2 = 150.5
* (n + 1) / 2 (F + 1) = 150.5 – (111 + 1) = 38.5

3. El ancho de los pasos iguales en la clase de la mediana
* (limiteSuperior – limiteInferior) / frecuencia = (349.5 – 300) / 72 = 0.6875
* m = 38.5 * 0.6875 + 300 = 326.468

4. El valor estimado de la mediana para estos datos

* Clase modal: 300 – 349.5 ~ 350 = 72
* d1 = frecuencia clase modal - frecuencia anterior = 72 - 58 = 14
* d2 = frecuencia clase modal - frecuencia siguiente = 72 - 63 = 9
* w = limite superior - limite inferior = 350 - 300 = 50
* Mo = LMo + (d1 / (d1 + d2)) * w = 300 + (14 / (14 + 9)) * 50 = 330

## Ejercicio

Las edades de una muestra de estudiantes que asisten a Sandhills Community College este semestre son:

| - | - | - | - | - | - | - | - | - | - |
|---|---|---|---|---|---|---|---|---|---|
| 19 | 17 | 15 | 20 | 23 | 41 | 33 | 21 | 18 | 20 |
| 18 | 33 | 32 | 29 | 24 | 19 | 18 | 20 | 17 | 22 |
| 55 | 19 | 22 | 25 | 28 | 30 | 44 | 19 | 20 | 39 |

1. Construya una distribución de frecuencias con intervalos `15-19`, `20-24`, `25-29`, `30-34` y `35 o más`

| clase | f = frecuencia | x = (lim sup - lim inf) / 2 | f * x |
|---|---|---|---|
| 15-19 | 10 | 17 | 170 |
| 20-24 | 9 | 22 | 198 |
| 25-29 | 3 | 27 | 81 |
| 30-34 | 4 | 32 | 128 |
| 35 o más | 4 | 37 | 148 |
| - | 30 | 135 | 725 |

### Media (2 formas de obtenerla)
* Más rápida: x = Σ(f * x) / n = 725 / 30 = 24.16
* Más exacta: x = Σx / n = (
  19 + 17 + 15 + 20 + 23 + 41 + 33 + 21 + 18 + 20 +
  18 + 33 + 32 + 29 + 24 + 19 + 18 + 20 + 17 + 22 +
  55 + 19 + 22 + 25 + 28 + 30 + 44 + 19 + 20 + 39
) / 30 = 760 / 30 = 25.3333333333

### Mediana (2 formas de obtenerla)
* w = limite superior - limite inferior = 24 - 20 = 4
* Más rápida: m = (n + 1) / 2 = (30 + 1) / 2 = 15.5
* Más exacta: m = { [ (30 + 1) / 2 - (10 + 1) ] / 2 } (4) + 20 = 22

### Moda
* d1 = frecuencia modal - frecuencia anterior = 10 - 0 = 10
* d2 = frecuencia modal - frecuencia siguiente = 10 - 9 = 1
* Mo = 15 + (10 / (10 + 1)) * 4 = 18.636363

## Segundo parcial

* La probabilidad es la posibilidad de que algo pase
* Se expresan como fracciones o como decimales, que estén entre 0 y 1
* Un evento es uno o más posibles resultados de hacer algo
* Un experimento es la actividad que origina dichos eventos

### Dados

<Dados display-text="Número de elementos" />

## Conjuntos

<Conjuntos :A='[1,2,3]' :B='[3,4,5]' />
<Conjuntos :A='["K",4,"F"]' :B='["F",4,6]' />

## Reglas de probabilidad

Por lo general se trabaja con 2 condiciones:
  * El caso que un evento u otro se presente
  * La situación en que 2 o más eventos se presenten al mismo tiempo

## Ejemplo

* Los registros policiacos muestran una media de 5 accidentes mensuales en cierta intersección
* Se desea saber la probabilidad de que en un mes, ocurran exactamente `0, 1, 2, 3 y 4` accidentes
  * P(0) = (5⁰ * e⁻⁵) / 0! = 0.006738
  * P(1) = (5¹ * e⁻⁵) / 1! = 0.03368
  * P(2) = (5² * e⁻⁵) / 2! = 0.08422
  * P(3) = (5³ * e⁻⁵) / 3! = 0.1403
  * P(4) = (5⁴ * e⁻⁵) / 4! = 0.1754
* ¿Cuál es la probabilidad de tener `0, 1 o 2` accidentes mensuales?
  * 0.006738 + 0.03368 + 0.08422 = 0.124638
* ¿Cuál es la probabilidad de tener `más de 3` accidentes mensuales?
  * P(x) > 3 = 1 - P(0) - P(1) - P(2) - P(3)
  * P(x) > 3 = 0.7353
