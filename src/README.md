---
home: true
footer: MIT Licensed | Powered by VuePress | Copyright @LuisEnMarroquin
---

<center>
  <h3>
    <a href='https://gitlab.com/LuisEnMarroquin/uvm' target='_blank'>gitlab.com/LuisEnMarroquin/uvm</a>
  </h3>
</center>

## Materias

### [Algebra](algebra.md)
### [English III](english.md)
### [Fisica](fisica.md)
### [Lógica de programación](logica.md)
### [Probabilidad y estadística](probabilidad.md)
### [Quimica](quimica.md)

## Plan de estudios

![alt text](./assets/study.jpg "Hover text")
