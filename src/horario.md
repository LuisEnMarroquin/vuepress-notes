# Horario

| Hora | Lunes | Martes | Miercoles | Jueves | Viernes |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 7:00 | F106 Física | F305 Química | F201 Física | - | - |
| 7:30 | F106 Física | F305 Química | F201 Física | - | - |
| 8:00 | F106 Física | F305 Química | F201 Física | - | - |
| 8:30 | F106 Física | F305 Química | F201 Física | - | - |
| 9:00 | F309 Lógica de programación | G106 Algebra | F309 Lógica de programación | G106 Algebra | G106 Algebra |
| 9:30 | F309 Lógica de programación | G106 Algebra | F309 Lógica de programación | G106 Algebra | G106 Algebra |
| 10:00 | F309 Lógica de programación | - | F309 Lógica de programación | - | F305 Química |
| 10:30 | G203 Probabilidad y estadística | - | G203 Probabilidad y estadística | - | F305 Química |
| 11:00 | G203 Probabilidad y estadística | - | G203 Probabilidad y estadística | - | F305 Química |
| 11:30 | G203 Probabilidad y estadística | - | G203 Probabilidad y estadística | - | F305 Química |
| 12:00 | - | - | - | - | - |
| 12:30 | - | - | - | - | - |
| 1:00 | - | - | - | - | - |
| 1:30 | - | - | - | - | - |
| 2:00 | - | G106 Inglés | - | G106 Inglés | - |
| 2:30 | - | G106 Inglés | - | G106 Inglés | - |
| 3:00 | - | G106 Inglés | - | G106 Inglés | G106 Inglés |
| 3:30 | - | G106 Inglés | - | G106 Inglés | G106 Inglés |
| Online | - | Z105 Taller de comunicación | - | - | - |
