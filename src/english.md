# English III

* Profesor: Enrique Montfort Larre
* Book: Touchstone 3

* Term grading criteria
* Theory 80%
* Laboratory 20%

* Theory grading criteria
* Written exam 40%
* Project 20%
* Online workbook 20%
* Portfolio 20%

* Lab grading criteria
* Communicative activities done in classroom

* Theory sessions attendance 80%
* Laboratory sessions attendance less than 3 skips

* Schoology: TX77-K9RJ-PDX33
* Do you have to slow down? - a a b a b a b b

## Complete these opinions with the correct forms of the words given

1.	Young people talk really fast and don’t speak clearly. And they use a lot of slang. It sounds terrible. They don’t always communicate well.
2.	People aren’t very patient when they must wait in long lines. They don’t speak to the clerks very politely either.
3.	Sometimes families argue because parents and children see thing differently.
4.	A lot of people automatically answer their cell phones when they ring, even at dinner. I think that’s just rude.
5.	People don’t feel safe on the roads because so many people are driving recklessly. Driving can be dangerous.
6.	A lot of people try hard to do their job carefully and thoroughly and they get stressed.

* Adverbs: AMLO is (so, too, very, mainly, incredibly, amazingly, really, totally, seriously) stupid.

![alt text](./assets/eng01.jpg "Hover text")

## Exercise D

1.	I am pretty disorganized. I am always losing things.
2.	Everyone in my family loves music.  We are always singing together.
3.	My brother is really generous with his time. He is always fixing my computer.
4.	My father is a workaholic. He is always coming home late. And he is always bringing work home with him.
5.	My college roommate was really funny. She was always making us laugh. You know she was always telling jokes.
6.	A friend of me is always complaining that she is broke, buy she is always buying expensive clothes.
7.	One of my friends is totally unreliable. He is always cancelling plans at the last minutes.

## Unit 3 - That's interesting

![alt text](./assets/eng02.jpg "Hover text")

### Part 1
1. Which is the busiest city in the world?
2. Which is the highest mountain in the world?
3. Which is the tallest building in the world?
4. Which is the longest river in the world?
5. Which is the largest desert in the world?

### Part 2
1. It's 300 m tall
2. It's 370 years old
3. It's 9.2 million km^2 large
4. It's 35 years long
5. It's 8858 m high
6. It's 93 m tall
7. It's 6650 km long
8. It's 21196.18 km long
9. It's 1642 m deep

### Part 3
| List of places | Would you recommend it? | Why? |
|---|---|---|
| Cancun, Quintana Roo | Yes | It's awesome and there is a lot to do |
| Progreso, Yucatan | Yes | It's relaxing and inexpensive |
| Mexico City, Mexico | No | They eat tortas de tamal and it's full of people |
| Saltillo, Coahuila | No | There is nothing to do |

### October 1, 2019
1. My dad made me think to get a new car
2. My mom is always telling me to eat more healthy
3. A friend is always telling me to learn French
4. Sometimes I give my parents my school grades
5. When I was younger my parents never let me stay out late

### Page 37 - Exercise 2
1. We'd go almost every summer
2. She'd do awesome tacos
3. They'd love to play
4. We'd watch TV all day
5. They'd used to enjoy the parties
6. We'd have a great day
7. We'd use to go to the Carl's
8. They'd enjoy the weekend

| Healthy | Unhealthy |
|---|---|
| Eggs | Chocolate |
| Lettuce | Donuts |
| Carrots | Soda |
| Meat | Ice cream |
| Rice | Candy |
| Fish | Chips |

## How often do you eat/drink?

| How often do you eat/drink? | Everyday | Twice a week | Once every 2 weeks | Once a month | Never |
|:--|:-:|:-:|:-:|:-:|:-:|
| Steamed vegetables | ✘ | ✘ | ✘ | ✘ | ✔ |
| Raw fish | ✘ | ✘ | ✘ | ✘ | ✔ |
| Noodles | ✘ | ✘ | ✘ | ✔ | ✘ |
| Roast chicken | ✘ | ✔ | ✘ | ✘ | ✘ |
| Tacos | ✔ | ✘ | ✘ | ✘ | ✘ |
| Soda | ✘ | ✔ | ✘ | ✘ | ✘ |
| Chocolate | ✘ | ✔ | ✘ | ✘ | ✘ |
| Pizza | ✘ | ✔ | ✘ | ✘ | ✘ |
| Popcorn | ✘ | ✘ | ✔ | ✘ | ✘ |
| Broccoli | ✘ | ✘ | ✘ | ✘ | ✔ |
| Sushi | ✘ | ✘ | ✘ | ✔ | ✘ |
| Pasta | ✘ | ✔ | ✘ | ✘ | ✘ |
| Cake | ✘ | ✘ | ✘ | ✔ | ✘ |
