# Física

* Profesor: Jorge Luis Arizpe Islas

* 1 kg Fuerza = 9.81 N
* Gravedad = 9.81 m/s^2
* Peso = w = m * gravedad

![alt text](./assets/fis01.jpg "Hover text")

* 1 camión puede transportar 15 yardas cúbicas de grava ¿Cuántos metros cúbicos puede cargar?
* 1 yarda = 3 pies
* 1 pie = 12 pulgadas
* 1 pulgada = 2.54 cm
* 1 m = 100 cm

![alt text](./assets/fis02.jpg "Hover text")

* 15 yo ^ 3 * 3 ft ^ 3 * 12 in ^ 3 * 2.54 cm ^ 3 / 100 cm ^ 3 = 11.46 m ^

| Energía | Fórmula |
|---|---|
| Cinetica | Ek = 1/2 mv² |
| Trabajo | w = Fxs |
| Potencial | E = mgh |
| Térmica | E = mcEAT |
| Cientica rotacional | E = 1/2 IW² |

* 30 HP => 2 = 22380W

* 1 HP / 746 HP
